package com.aait.carwash.UI.Activities.AppInfo

import android.content.Intent
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Provider.MainActivity
import com.aait.carwash.UI.Controllers.BankAdapter

import com.aait.carwash.UI.Controllers.BanksAdapter
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class BankActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_bank
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var total:TextView
    lateinit var banks:RecyclerView
    lateinit var owner:EditText
    lateinit var bank_name:EditText
    lateinit var amount:EditText
    lateinit var image:TextView
    lateinit var send:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var banksAdapter: BankAdapter
    var banksModels = ArrayList<BankModel>()
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var price = ""
    override fun initializeComponents() {
        price = intent.getStringExtra("price")!!
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        total = findViewById(R.id.total)
        banks = findViewById(R.id.banks)
        owner = findViewById(R.id.owner)
        bank_name = findViewById(R.id.bank_name)
        amount = findViewById(R.id.amount)
        image = findViewById(R.id.image)
        send = findViewById(R.id.send)
        total.text = getString(R.string.total)+price+getString(R.string.rs)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.bank_accounts)
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        banksAdapter = BankAdapter(mContext,banksModels,R.layout.recycle_banks)
        banks.layoutManager = linearLayoutManager
        banks.adapter = banksAdapter
        getData()

        send.setOnClickListener {
            if (CommonUtil.checkEditError(owner,getString(R.string.account_owner))||
                    CommonUtil.checkEditError(bank_name,getString(R.string.bank_name))||
                    CommonUtil.checkEditError(amount,getString(R.string.paid_amount))||
                    CommonUtil.checkTextError(image,getString(R.string.add_iamge))){
                return@setOnClickListener
            }else{
                upLoad(ImageBasePath!!)

            }
        }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Accounts("Bearer"+user.userData.token,lang.appLanguage)
                ?.enqueue(object : Callback<BankResponse> {
                    override fun onFailure(call: Call<BankResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<BankResponse>, response: Response<BankResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                banksAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun upLoad(path:String) {
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.Pay("Bearer"+user.userData.token,lang.appLanguage,owner.text.toString()
        ,bank_name.text.toString(),amount.text.toString(),filePart)?.enqueue(object :Callback<BanksResponse>{
            override fun onFailure(call: Call<BanksResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BanksResponse>, response: Response<BanksResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@BankActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]



                if (ImageBasePath != null) {

                    image.text = ImageBasePath
                }
            }
        }

    }
}