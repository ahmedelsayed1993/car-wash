package com.aait.carwash.UI.Activities.Main.Provider

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.OrderDetailsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Client.InvoiceActivty
import com.aait.carwash.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RefuseActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_refuse
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var image: ImageView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var invoice: TextView
    lateinit var order_num: TextView
    lateinit var phone: TextView
    lateinit var type: TextView
    lateinit var time: TextView
    lateinit var payment: TextView
    lateinit var date: TextView
    lateinit var notes: TextView
    lateinit var refuse: Button
    lateinit var reason:EditText
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        invoice = findViewById(R.id.invoice)
        order_num = findViewById(R.id.order_num)
        phone = findViewById(R.id.phone)
        type = findViewById(R.id.type)
        time = findViewById(R.id.time)
        payment = findViewById(R.id.payment)
        date = findViewById(R.id.date)
        notes = findViewById(R.id.notes)
        reason = findViewById(R.id.reason)
        refuse = findViewById(R.id.refuse)

        title.text = getString(R.string.order_details)
        back.setOnClickListener { onBackPressed()
            finish()}
        invoice.setOnClickListener {
            val intent = Intent(this, InvoiceActivty::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }
        getData()
        refuse.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.ProviderOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,reason.text.toString(),"canceled")
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@RefuseActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,null)
                ?.enqueue(object : Callback<OrderDetailsResponse> {
                    override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.image).into(image)
                                name.text = response.body()?.data?.user_name
                                address.text = response.body()?.data?.address
                                date.text = getString(R.string.date)+":"+response.body()?.data?.date
                                time.text = getString(R.string.time)+":"+response.body()?.data?.time
                                order_num.text = getString(R.string.order_num)+":"+response.body()?.data?.order_id
                                phone.text = getString(R.string.phone_number)+":"+response.body()?.data?.phone
                                type.text = getString(R.string.subscribe)+":"+response.body()?.data?.subtype
                                payment.text = getString(R.string.payment_method)+":"+response.body()?.data?.payment_method
                                notes.text = getString(R.string.notes)+":"+response.body()?.data?.desc

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}