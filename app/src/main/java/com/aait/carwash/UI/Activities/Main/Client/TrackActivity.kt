package com.aait.carwash.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.GPS.GPSTracker
import com.aait.carwash.GPS.GpsTrakerListener
import com.aait.carwash.Models.TrackOrder
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.DialogUtil
import com.aait.carwash.Utils.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class TrackActivity:ParentActivity(), OnMapReadyCallback,
        GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_track
    var id = 0
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var map:MapView
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false

    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 60000L
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        map = findViewById(R.id.map)
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        back.setOnClickListener {
            onBackPressed()
            finish()
            timer.cancel()
        }
        title.text = getString(R.string.follow_order)

    }

    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0!!
        getLocationWithPermission()
    }

    override fun onResume() {
        super.onResume()

        val timerTask = object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    Log.e("rrr","rrrrrr")
                    track() }
            }
        }

        timer = Timer()
        timer.schedule(timerTask, noDelay, everyFiveSeconds)
    }

    override fun onPause() {
        super.onPause()

//        timer.cancel()
//        timer.purge()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        timer.cancel()
    }
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.placeholder))
        )!!
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }
    fun putMapMarker1(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin_two))
        )!!
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }


    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
              //  putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }

    fun track(){
        Client.getClient()?.create(Service::class.java)?.Track(lang.appLanguage,"Bearer"+user.userData.token,id)
                ?.enqueue(object : Callback<TrackOrder> {
                    override fun onFailure(call: Call<TrackOrder>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<TrackOrder>, response: Response<TrackOrder>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                putMapMarker1(response.body()?.data?.get(0)?.lat_provider!!.toDouble(),response.body()?.data?.get(0)?.lng_provider!!.toDouble())
                                putMapMarker(response.body()?.data?.get(0)?.lat_user!!.toDouble(),response.body()?.data?.get(0)?.lng_user!!.toDouble())
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }



}