package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Provider.MainActivity
import com.aait.carwash.UI.Activities.Main.Provider.OfferOrderDetailsActivity
import com.aait.carwash.UI.Activities.Main.Provider.OrderDetailsActivity

import com.aait.carwash.UI.Controllers.NotificationAdapter
import com.aait.carwash.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_notification
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var notificationModels = java.util.ArrayList<NotificationModel>()
    internal lateinit var notificationAdapter: NotificationAdapter
    lateinit var back: ImageView
    lateinit var title: TextView

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)

        title.text = getString(R.string.notification)
        back.setOnClickListener {if (user.userData.type.equals("provider")) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }else{
            val intent = Intent(this, com.aait.carwash.UI.Activities.Main.Client.MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        }

        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        notificationAdapter =  NotificationAdapter(mContext,notificationModels,R.layout.recycle_notification)
        notificationAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = notificationAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()


    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Notification(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
            Callback<NotificationResponse> {
            override fun onResponse(call: Call<NotificationResponse>, response: Response<NotificationResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                notificationAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete) {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.delete(
                "Bearer"+user.userData.token,lang.appLanguage
                , notificationModels.get(position).id!!
            )?.enqueue(object :
                Callback<BaseResponse> {
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            getHome()
                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

            })
        }else {if (user.userData.type.equals("provider")) {

            if (notificationModels.get(position).type.equals("order")) {

                val intent = Intent(this, OrderDetailsActivity::class.java)
                intent.putExtra("id", notificationModels.get(position).order_id!!.toInt())
                startActivity(intent)

            }else if (notificationModels.get(position).type.equals("subscriber")){
                val intent = Intent(this, OfferOrderDetailsActivity::class.java)
                intent.putExtra("id", notificationModels.get(position).order_id!!.toInt())
                startActivity(intent)
            }
        }else{
            if (notificationModels.get(position).type.equals("order")) {

                val intent = Intent(this, com.aait.carwash.UI.Activities.Main.Client.OrderDetailsActivity::class.java)
                intent.putExtra("id", notificationModels.get(position).order_id!!.toInt())
                startActivity(intent)

            }
        }
        }
    }
}