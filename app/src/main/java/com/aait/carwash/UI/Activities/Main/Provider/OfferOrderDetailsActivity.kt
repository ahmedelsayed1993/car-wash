package com.aait.carwash.UI.Activities.Main.Provider

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.OfferSubscribeResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OfferOrderDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_offer_order_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var pack_name:TextView
    lateinit var owner:TextView
    lateinit var bank_name:TextView
    lateinit var amount:TextView
    lateinit var teransfer_image:ImageView
    lateinit var accept:Button
    lateinit var refuse:Button
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        pack_name = findViewById(R.id.pack_name)
        owner = findViewById(R.id.owner)
        bank_name = findViewById(R.id.bank_name)
        amount = findViewById(R.id.amount)
        teransfer_image = findViewById(R.id.transfer_image)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.packages_orders)
        getData()
        accept.setOnClickListener {   showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OfferSub("Bearer"+user.userData.token,lang.appLanguage,id,"accepted")
                    ?.enqueue(object : Callback<OfferSubscribeResponse>{
                        override fun onFailure(call: Call<OfferSubscribeResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<OfferSubscribeResponse>, response: Response<OfferSubscribeResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                    startActivity(Intent(this@OfferOrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })}
        refuse.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OfferSub("Bearer"+user.userData.token,lang.appLanguage,id,"refuse")
                    ?.enqueue(object : Callback<OfferSubscribeResponse>{
                        override fun onFailure(call: Call<OfferSubscribeResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<OfferSubscribeResponse>, response: Response<OfferSubscribeResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                    startActivity(Intent(this@OfferOrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OfferSub("Bearer"+user.userData.token,lang.appLanguage,id,null)
                ?.enqueue(object : Callback<OfferSubscribeResponse>{
                    override fun onFailure(call: Call<OfferSubscribeResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<OfferSubscribeResponse>, response: Response<OfferSubscribeResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.image_user).into(image)
                                name.text = response.body()?.data?.user_name
                                pack_name.text = getString(R.string.package_type)+":"+response.body()?.data?.offer_name
                                owner.text = getString(R.string.account_owner)+":"+response.body()?.data?.account_holder_name
                                bank_name.text = getString(R.string.bank_name)+":"+response.body()?.data?.bank_name
                                amount.text = getString(R.string.paid_amount)+":"+response.body()?.data?.amount+getString(R.string.rs)
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.image_subscriber).into(teransfer_image)
                                if (response.body()?.data?.status.equals("new")){
                                    accept.visibility = View.VISIBLE
                                    refuse.visibility = View.VISIBLE
                                }else{
                                    accept.visibility = View.GONE
                                    refuse.visibility = View.GONE
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}