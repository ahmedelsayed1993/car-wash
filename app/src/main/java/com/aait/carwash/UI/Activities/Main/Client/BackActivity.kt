package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.R

class BackActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_done
    lateinit var back:TextView

    override fun initializeComponents() {
        back = findViewById(R.id.bac)

        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}