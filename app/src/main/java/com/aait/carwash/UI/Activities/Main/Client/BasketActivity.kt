package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Cart.AllCartViewModel
import com.aait.carwash.Cart.CartDataBase
import com.aait.carwash.Cart.ProviderModelOffline
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.CartProviderAdapter

import com.aait.carwash.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BasketActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_basket
    lateinit var back:ImageView
    lateinit var title:TextView

    lateinit var rv_recycle: RecyclerView

    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var providers = ArrayList<ProviderModelOffline>()

    lateinit var linearLayoutManager: LinearLayoutManager
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    lateinit var cartProviderAdapter: CartProviderAdapter
    var tax = ""
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        back.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
            finish()}
       // title.text = getString(R.string.shopping_basket)


        rv_recycle = findViewById(R.id.rv_recycle)

        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)

        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cartProviderAdapter = CartProviderAdapter(mContext,providers,R.layout.recycle_basket)
        cartProviderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = cartProviderAdapter
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){
                    layNoItem!!.visibility = View.VISIBLE

                }else {
                   layNoItem!!.visibility = View.GONE
                    for (i in 0..cartModels.size-1){
                        providers.add(cartModels.get(i))

                    }


                     cartProviderAdapter.updateAll(cartModels)
                    Log.e("cart", Gson().toJson(cartModels))


                }


            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete){
            allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
                override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                    cartModelOfflines = cartModels


                        for (i in 0..cartModels.size-1){
                            if (cartModels.get(i).service_id==providers.get(position).service_id) {
                                allCartViewModel!!.deleteItem(cartModels.get(i))
                            }

                        }

//                        Log.e("prov", Gson().toJson(providers.distinctBy { it.provider_id }))
//                        cartProviderAdapter.updateAll(providers.distinctBy { it.provider_id })
                        Log.e("cart", Gson().toJson(cartModels))





                }
            })

            //allCartViewModel!!.deleteItem(cartModelOfflines.get(position))
        }else {
            val intent = Intent(this, CartDetailsActivity::class.java)
            intent.putExtra("provider_id", providers.get(position).provider_id)
            intent.putExtra("provider_name", providers.get(position).provider_name)
            intent.putExtra("provider_image", providers.get(position).provider_image)
            intent.putExtra("address", providers.get(position).address)
            intent.putExtra("count", providers.get(position).count)
            intent.putExtra("name", providers.get(position).name)
            intent.putExtra("service_id", providers.get(position).service_id)
            intent.putExtra("type_id", providers.get(position).type_id)
            startActivity(intent)
        }

    }


}