package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.OffersModel
import com.aait.carwash.Models.OrdersModel
import com.aait.carwash.R
import com.bumptech.glide.Glide

class OfferAdapter (context: Context, data: MutableList<OffersModel>, layoutId: Int) :
        ParentRecyclerAdapter<OffersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        viewHolder.content!!.setText(questionModel.content)
        viewHolder.from.text = questionModel.start
        viewHolder.to.text = questionModel.end
        viewHolder.discount.text = mcontext.getString(R.string.discount)+questionModel.discount+"%"

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {






        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var discount = itemView.findViewById<TextView>(R.id.discount)
        internal var from = itemView.findViewById<TextView>(R.id.from)
        internal var to = itemView.findViewById<TextView>(R.id.to)
        internal var content = itemView.findViewById<TextView>(R.id.content)



    }
}