package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Cart.AllCartViewModel
import com.aait.carwash.Cart.CartDataBase
import com.aait.carwash.Cart.ProviderModelOffline
import com.aait.carwash.Models.TimeResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Auth.LoginActivity
import com.aait.carwash.UI.Controllers.CartProviderAdapter
import com.aait.carwash.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_cart_details
    lateinit var back:ImageView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var order:Button
    lateinit var cancel:Button
    var provider_id = 0
    var provider_name=""
    var provider_image= ""
    var address_ = ""
    var type_id = 0
    var name_ = ""
    var service_id = 0
    var count = 0
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    lateinit var cartProviderAdapter: CartProviderAdapter

    override fun initializeComponents() {
        provider_id = intent.getIntExtra("provider_id",0)
        provider_image = intent.getStringExtra("provider_image")!!
        provider_name = intent.getStringExtra("provider_name")!!
        address_ = intent.getStringExtra("address")!!
        type_id = intent.getIntExtra("type_id",0)
        name_ = intent.getStringExtra("name")!!
        service_id = intent.getIntExtra("service_id",0)
        count = intent.getIntExtra("count",0)
        back = findViewById(R.id.back)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        order = findViewById(R.id.order)
        cancel = findViewById(R.id.cancel)
        back.setOnClickListener { onBackPressed()
        finish()}
        Glide.with(mContext).load(provider_image).into(image)
        name.text = provider_name
        address.text = address_
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){

                }else {

                    Log.e("cart", Gson().toJson(cartModels))


                }


            }
        })
        order.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, CompleteCartOrder::class.java)
                intent.putExtra("provider_id", provider_id)
                intent.putExtra("provider_name", provider_name)
                intent.putExtra("provider_image", provider_image)
                intent.putExtra("address", address_)
                intent.putExtra("count", count)
                intent.putExtra("name", name_)
                intent.putExtra("service_id", service_id)
                intent.putExtra("type_id", type_id)
                startActivity(intent)
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra("type","user")
                startActivity(intent)
            }
        }
        cancel.setOnClickListener {
            allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
                override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                    cartModelOfflines = cartModels


                    for (i in 0..cartModels.size-1){
                        if (cartModels.get(i).service_id==service_id) {
                            allCartViewModel!!.deleteItem(cartModels.get(i))
                            onBackPressed()
                            finish()
                        }else{
                            onBackPressed()
                            finish()
                        }

                    }

//                        Log.e("prov", Gson().toJson(providers.distinctBy { it.provider_id }))
//                        cartProviderAdapter.updateAll(providers.distinctBy { it.provider_id })
                    Log.e("cart", Gson().toJson(cartModels))





                }
            })
        }

    }

}