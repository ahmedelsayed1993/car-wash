package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Cart.AddCartViewModel
import com.aait.carwash.Cart.AllCartViewModel
import com.aait.carwash.Cart.CartDataBase
import com.aait.carwash.Cart.ProviderModelOffline
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.ProductsAdapter
import com.aait.carwash.UI.Controllers.ServiceAdapter
import com.aait.carwash.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_products
     lateinit var servicesModel: ServicesModel
    lateinit var providerDetailsModel: ProviderDetailsModel
    lateinit var back: ImageView
    lateinit var title: TextView

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var order: Button
    lateinit var cart:Button
    lateinit var productsAdapter: ProductsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var products = ArrayList<ProductModel>()
    lateinit var productModel: ProductModel
    override lateinit var addCartViewModel: AddCartViewModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartDataBase: CartDataBase
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    override fun initializeComponents() {
        servicesModel = intent.getSerializableExtra("service") as ServicesModel
        providerDetailsModel = intent.getSerializableExtra("provider") as ProviderDetailsModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        order = findViewById(R.id.order)
        cart = findViewById(R.id.cart)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.service_type)
        productsAdapter = ProductsAdapter(mContext, products, R.layout.recycle_product)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        productsAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = productsAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        cartDataBase = CartDataBase.getDataBase(mContext)
        addCartViewModel = ViewModelProviders.of(this).get(AddCartViewModel::class.java)
       // allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        addCartViewModel = AddCartViewModel(application)
        order.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, CompleteOrderActivity::class.java)
                intent.putExtra("provider", providerDetailsModel)
                intent.putExtra("service", servicesModel)
                intent.putExtra("product", productModel)
                startActivity(intent)
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
            }
        }
        cart.setOnClickListener {
            addCartViewModel.addItem(ProviderModelOffline(providerDetailsModel.provider_id!!,
                    providerDetailsModel.name!!,
                    providerDetailsModel.avatar!!,
                    providerDetailsModel.address!!,
                    productModel.id!!,servicesModel.name!!,servicesModel.service_id!!,servicesModel.count!!
            ))
            CommonUtil.makeToast(mContext,getString(R.string.added_succefully))
            onBackPressed()
            finish()
        }

    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Products(lang.appLanguage,providerDetailsModel.provider_id!!,servicesModel.service_id!!)?.enqueue(object:
                Callback<ProductResponse> {
            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<ProductResponse>,
                    response: Response<ProductResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            order.visibility = View.GONE
                            cart.visibility = View.GONE

                        } else {
                            order.visibility = View.VISIBLE
                            cart.visibility = View.VISIBLE
                            productsAdapter.updateAll(response.body()?.data!!)
                            productModel = response.body()?.data?.get(0)!!
                        }


                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.lay){
            productsAdapter.selected = position
            products.get(position).checked = true
            productsAdapter.notifyDataSetChanged()
            productModel = products.get(position)
            Log.e("product", Gson().toJson(productModel))
        }
    }
}