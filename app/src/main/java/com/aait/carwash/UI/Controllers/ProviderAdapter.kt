package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.ProvidersModel
import com.aait.carwash.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class ProviderAdapter (context: Context, data: MutableList<ProvidersModel>, layoutId: Int) :
        ParentRecyclerAdapter<ProvidersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        Glide.with(mcontext).asBitmap().load(listModel.avatar).into(viewHolder.image)
        viewHolder.name.text = listModel.name
        viewHolder.address.text = listModel.address
        viewHolder.distance.text = listModel.distance
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var distance = itemView.findViewById<TextView>(R.id.distance)


    }
}