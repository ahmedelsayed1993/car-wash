package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.OffersModel
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class TransferActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_transfer
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var name:EditText
    lateinit var bank_name:EditText
    lateinit var amount:EditText
    lateinit var image:ImageView
    lateinit var send:Button
    var id = 0
   lateinit var offersModel:OffersModel
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        offersModel = intent.getSerializableExtra("package") as OffersModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        bank_name = findViewById(R.id.bank_name)
        amount = findViewById(R.id.amount)
        image = findViewById(R.id.image)
        send = findViewById(R.id.send)
        amount.setText(offersModel.price)

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        title.text = getString(R.string.bank_accounts)
        back.setOnClickListener { onBackPressed()
        finish()}
        send.setOnClickListener {  if (CommonUtil.checkEditError(name,getString(R.string.account_owner))||
                CommonUtil.checkEditError(bank_name,getString(R.string.bank_name))||
                CommonUtil.checkEditError(amount,getString(R.string.paid_amount))){
            return@setOnClickListener
        }else{
            if (ImageBasePath!=null) {
                upLoad(ImageBasePath!!)
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.add_iamge))
            }

        } }
    }
    fun upLoad(path:String) {
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddOffer("Bearer"+user.userData.token,lang.appLanguage,id,offersModel.id!!,bank_name.text.toString()
                ,name.text.toString(),amount.text.toString(),filePart)?.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@TransferActivity,BackActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]



                if (ImageBasePath != null) {

                    Glide.with(mContext).load(ImageBasePath).into(image)
                }
            }
        }

    }
}