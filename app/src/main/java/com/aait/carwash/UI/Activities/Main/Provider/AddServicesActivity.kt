package com.aait.carwash.UI.Activities.Main.Provider

import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.SizesAdapter
import com.aait.carwash.UI.Controllers.TypesAdapter
import com.aait.carwash.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddServicesActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_add_services
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var sizes:RecyclerView
    lateinit var types:RecyclerView
    lateinit var confirm:Button
    lateinit var sizesAdapter: SizesAdapter
    lateinit var typesAdapter: TypesAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var gridLayoutManager1: GridLayoutManager
    var sizesModels = ArrayList<SizesModel>()
    var typesModels = ArrayList<TypesModel>()
    var size = 0
    var type = ArrayList<Type>()
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        sizes = findViewById(R.id.sizes)
        types = findViewById(R.id.types)
        confirm = findViewById(R.id.confirm)
        gridLayoutManager = GridLayoutManager(mContext,2)
        gridLayoutManager1 = GridLayoutManager(mContext,2)
        sizesAdapter = SizesAdapter(mContext,sizesModels,R.layout.recycle_service)
        sizesAdapter.setOnItemClickListener(this)
        sizes.layoutManager = gridLayoutManager
        sizes.adapter = sizesAdapter
        typesAdapter = TypesAdapter(mContext,typesModels,R.layout.recycle_types)
        types.layoutManager = gridLayoutManager1
        types.adapter = typesAdapter
        title.text = getString(R.string.add_service)
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()
        confirm.setOnClickListener {
            for (i in 0..typesAdapter.data.size-1){
                if (typesAdapter.data.get(i).price.equals("")){

                }else{
                    type.add(Type(typesAdapter.data.get(i).id,typesAdapter.data.get(i).price!!.toInt()))
                }

            }
            Log.e("types",Gson().toJson(type))
            if (type.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.cleaning_type))
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.AddService("Bearer"+user.userData.token,lang.appLanguage,size,Gson().toJson(type)
                )?.enqueue(object :Callback<BaseResponse>{
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                onBackPressed()
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Services("Bearer"+user.userData.token,lang.appLanguage)
                ?.enqueue(object : Callback<AddServiceResponse> {
                    override fun onFailure(call: Call<AddServiceResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<AddServiceResponse>, response: Response<AddServiceResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                size = response.body()?.data?.size?.get(0)?.id!!
                                sizesAdapter.updateAll(response.body()?.data?.size!!)
                                typesAdapter.updateAll(response.body()?.data?.typeCleanings!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        sizesAdapter.selected = position
        sizesModels.get(position).checked = true
        sizesAdapter.notifyDataSetChanged()
        size = sizesModels.get(position).id!!
    }
}