package com.aait.carwash.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.ContactUsResponse
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R

import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComplaintActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_complaint
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var name: EditText
    lateinit var phone: EditText
    lateinit var email: EditText
    lateinit var msg_title: EditText
    lateinit var msg_text: EditText
    lateinit var send: Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        msg_text = findViewById(R.id.msg_text)
        msg_title = findViewById(R.id.msg_title)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.suggestions_complaints)

        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkEditError(msg_title,getString(R.string.complaint_title))||
                    CommonUtil.checkEditError(msg_text,getString(R.string.complaint_text))){
                return@setOnClickListener
            }else{
                SendData()
            }
        }
    }
    fun SendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Complaint(lang.appLanguage,name.text.toString(),phone.text.toString(),email.text.toString(),msg_title.text.toString(),msg_text.text.toString())
                ?.enqueue(object : Callback<BaseResponse> {
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                onBackPressed()
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}