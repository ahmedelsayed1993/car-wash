package com.aait.carwash.UI.Activities.Main.Provider

import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.TypesAdapter
import com.aait.carwash.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditServiceActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_edit_service
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var services:RecyclerView
    lateinit var confirm: Button
    lateinit var typesAdapter: TypesAdapter
    lateinit var gridLayoutManager1: GridLayoutManager
    var type = ArrayList<Type>()
    var typesModels = ArrayList<TypesModel>()
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        services = findViewById(R.id.services)
        confirm = findViewById(R.id.confirm)
        gridLayoutManager1 = GridLayoutManager(mContext,2)
        typesAdapter = TypesAdapter(mContext,typesModels,R.layout.recycle_types)
        services.layoutManager = gridLayoutManager1
        services.adapter = typesAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()
        confirm.setOnClickListener {
            for (i in 0..typesAdapter.data?.size-1){
                type.add(Type(typesAdapter.data?.get(i).id,typesAdapter.data?.get(i).price!!.toInt()))
            }
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.UpdateServicce("Bearer"+user.userData.token,lang.appLanguage,id,Gson().toJson(type))
                    ?.enqueue(object :Callback<BaseResponse>{
                        override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    onBackPressed()
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                        override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                    })
        }


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditService("Bearer"+user.userData.token,lang.appLanguage,id)
                ?.enqueue(object : Callback<EditServiceResponse> {
                    override fun onResponse(call: Call<EditServiceResponse>, response: Response<EditServiceResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                typesAdapter.updateAll(response.body()?.data?.typeCleanings!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<EditServiceResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                })
    }
}