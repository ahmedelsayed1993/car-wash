package com.aait.carwash.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.*
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.NotifyResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Provider.DeliveryActivity
import com.aait.carwash.UI.Activities.SplashActivity
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderSettingsActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_settings
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var notifi: Switch
    lateinit var lay: LinearLayout
    lateinit var language: RadioGroup
    lateinit var arabic: RadioButton
    lateinit var english: RadioButton
    lateinit var delivery:LinearLayout
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        notifi = findViewById(R.id.notifi)
        lay = findViewById(R.id.lay)
        language = findViewById(R.id.language)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        delivery = findViewById(R.id.delivery)
        title.text = getString(R.string.settings)
        back.setOnClickListener { onBackPressed()
            finish()}
        delivery.setOnClickListener { startActivity(Intent(this,DeliveryActivity::class.java)) }

        // notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        if (user.loginStatus!!){
            notify(null)
        }else{

        }
        notifi.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (isChecked) {
                // theme.appTheme =
                if (user.loginStatus!!){
                    notify(1)
                }

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }else{
                //  theme.appTheme =
                if (user.loginStatus!!){
                    notify(0)
                }

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }
        })
        lay.setOnClickListener {
            if (language.visibility == View.VISIBLE){

            }else{
                language.visibility = View.VISIBLE
            }
        }
        if (lang.appLanguage=="ar"){
            arabic.isChecked = true
        }else{
            english.isChecked = true
        }
        arabic.setOnClickListener { lang.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))}
        english.setOnClickListener { lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))}
    }
    fun notify(swit:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Switch("Bearer"+user.userData.token,swit)?.enqueue(object : Callback<NotifyResponse> {
            override fun onFailure(call: Call<NotifyResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<NotifyResponse>, response: Response<NotifyResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            notifi.isChecked = true
                        }else{
                            notifi.isChecked = false
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}