package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.BankModel
import com.aait.carwash.Models.BanksModel
import com.aait.carwash.R

class BankAdapter (context: Context, data: MutableList<BankModel>, layoutId: Int) :
        ParentRecyclerAdapter<BankModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.bank_name.text = "  "+questionModel.bank_name
        viewHolder.account_name.text = "  "+questionModel.account_number
        viewHolder.account_number.text = "  "+questionModel.bank_account
        viewHolder.iban_name.text = "  "+questionModel.account_ibn
        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)

//        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
//
//        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var bank_name=itemView.findViewById<TextView>(R.id.bank_name)
        internal var account_name = itemView.findViewById<TextView>(R.id.account_name)
        internal var account_number = itemView.findViewById<TextView>(R.id.account_number)
        internal var iban_name = itemView.findViewById<TextView>(R.id.iban_name)




    }
}