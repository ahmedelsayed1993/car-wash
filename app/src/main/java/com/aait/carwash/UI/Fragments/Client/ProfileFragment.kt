package com.aait.carwash.UI.Fragments.Client

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.aait.carwash.Base.BaseFragment
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Auth.ProviderRegisterActivity
import com.aait.carwash.UI.Activities.Main.Client.ProfileActivity

class ProfileFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_profile
    companion object {
        fun newInstance(): ProfileFragment {
            val args = Bundle()
            val fragment = ProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var profile:TextView
    lateinit var provider:TextView
    override fun initializeComponents(view: View) {
        profile = view.findViewById(R.id.profile)
        provider = view.findViewById(R.id.provider)

        provider.setOnClickListener { startActivity(Intent(activity,ProviderRegisterActivity::class.java)) }
        profile.setOnClickListener { startActivity(Intent(activity,ProfileActivity::class.java)) }

    }
}