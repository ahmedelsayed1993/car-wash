package com.aait.carwash.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.UserResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
//import com.aait.carwash.Models.UserResponse
//import com.aait.carwash.Network.Client
//import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_forgot_pass
    lateinit var phone: EditText
    lateinit var confirm:Button

    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        confirm = findViewById(R.id.send)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))){
                return@setOnClickListener
            }else{

                ForgotPass()
            }
        }

    }

    fun ForgotPass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ForGot(phone.text.toString(),lang.appLanguage)?.enqueue(object :
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        val intent =
                                Intent(this@ForgotPasswordActivity, NewPassActivity::class.java)
                        intent.putExtra("data", response.body()?.data)
                        startActivity(intent)
                        finish()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })

    }
}