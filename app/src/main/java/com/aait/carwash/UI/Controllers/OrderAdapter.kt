package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.OrdersModel
import com.aait.carwash.R

import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class OrderAdapter (context: Context, data: MutableList<OrdersModel>, layoutId: Int) :
        ParentRecyclerAdapter<OrdersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        viewHolder.address!!.setText(questionModel.address)
        viewHolder.num.text = questionModel.order_id.toString()
        Glide.with(mcontext).asBitmap().load(questionModel.image).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var num = itemView.findViewById<TextView>(R.id.order_num)



    }
}