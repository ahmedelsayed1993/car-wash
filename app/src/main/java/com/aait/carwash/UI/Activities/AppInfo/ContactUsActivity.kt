package com.aait.carwash.UI.Activities.AppInfo

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.ContactUsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R

import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_contact_us
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var numbers:TextView
    lateinit var snap:ImageView
    lateinit var twitter:ImageView
    lateinit var instagram:ImageView
    lateinit var face:ImageView
    lateinit var linked:ImageView
    lateinit var whats:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var msg_title:EditText
    lateinit var msg_text:EditText
    lateinit var send:Button
    var snapchet = ""
    var twite = ""
    var facebook = ""
    var link = ""
    var what = ""
    var insta = ""

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        face = findViewById(R.id.face)
        linked = findViewById(R.id.linked)
        whats = findViewById(R.id.whats)
        numbers = findViewById(R.id.numbers)
        snap= findViewById(R.id.snap)
        twitter = findViewById(R.id.twitter)
        instagram = findViewById(R.id.instagram)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        msg_text = findViewById(R.id.msg_text)
        msg_title = findViewById(R.id.msg_title)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
            finish()
            }
        title.text = getString(R.string.contact_us)
        
        getData()




        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkEditError(msg_title,getString(R.string.message_title))||
                    CommonUtil.checkEditError(msg_text,getString(R.string.message_text))){
                return@setOnClickListener
            }else{
                SendData()
            }
        }

        twitter.setOnClickListener { if (!twite.equals(""))
        {
            if (twite.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(twite)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$twite"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        face.setOnClickListener { if (!facebook.equals(""))
        {
            if (facebook.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(facebook)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$facebook"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        linked.setOnClickListener { if (!link.equals(""))
        {
            if (link.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(link)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$link"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        whats.setOnClickListener {  what.replaceFirst("0","+966",false)
            openWhatsAppConversationUsingUri(mContext!!,what,"")  }
        instagram.setOnClickListener { if (!insta.equals(""))
        {
            if (insta.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(insta)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$insta"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }  }
        snap.setOnClickListener { if (!snapchet.equals(""))
        {
            if (snapchet.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(snapchet)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$snapchet"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }  }


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage)
                ?.enqueue(object : Callback<ContactUsResponse>{
                    override fun onFailure(call: Call<ContactUsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<ContactUsResponse>, response: Response<ContactUsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                numbers.text = response.body()?.data?.numbers
                                for (i in 0..response.body()?.socials!!.size-1){
                                    if (response.body()?.socials?.get(i)?.name.equals("snapchat")){
                                        snapchet = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("instagram")){
                                        insta = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("twitter")){
                                        twite = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("facebook")){
                                        facebook = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("linkedin")){
                                        link = response.body()?.socials?.get(i)?.link!!
                                    }else if (response.body()?.socials?.get(i)?.name.equals("whatsapp")){
                                        what = response.body()?.socials?.get(i)?.link!!
                                    }
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun openWhatsAppConversationUsingUri(
            context: Context,
            numberWithCountryCode: String,
            message: String
    ) {

        val uri =
                Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }
    fun SendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ContactUs(lang.appLanguage,name.text.toString(),phone.text.toString(),email.text.toString(),msg_title.text.toString(),msg_text.text.toString())
                ?.enqueue(object : Callback<BaseResponse>{
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                              CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                onBackPressed()
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}