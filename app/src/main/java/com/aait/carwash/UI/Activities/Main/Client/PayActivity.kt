package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_pay

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var cash:RadioButton
    lateinit var mada:RadioButton
    lateinit var visa:RadioButton
    lateinit var confirm:Button
    var pay = "cash"
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        cash = findViewById(R.id.cash)
        mada = findViewById(R.id.mada)
        visa = findViewById(R.id.visa)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.payment_method)
        cash.setOnClickListener {
            pay = "cash"
        }
        mada.setOnClickListener { pay = "mada" }
        visa.setOnClickListener { pay = "visa" }
        confirm.setOnClickListener {
            if (pay.equals("mada")){
                val intent = Intent(this,PayOnlineActivity::class.java)
                    intent.putExtra("id",id)
                    intent.putExtra("type","mada")
                    startActivity(intent)
                }else if (pay.equals("visa")){
                val intent = Intent(this,PayOnlineActivity::class.java)
                intent.putExtra("id",id)
                intent.putExtra("type","visa")
                startActivity(intent)
            }
            else {
                pay()
            }
        }

    }
    fun pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Pay("Bearer"+user.userData.token,lang.appLanguage,id,pay
        )?.enqueue(object : Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@PayActivity,BackActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}