package com.aait.carwash.UI.Controllers

import java.io.Serializable

class UserOrderDetailsModel:Serializable {
    var reason:String?=null
    var status:String?=null
    var image:String?=null
    var address:String?=null
    var name:String?=null
    var provider_id:Int?=null
    var order_id:Int?=null
    var rate:Int?=null
    var phone:String?=null
}