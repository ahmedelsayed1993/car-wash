package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_rate
     lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var rating:RatingBar
    lateinit var confirm:Button
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rating = findViewById(R.id.rating)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.rate_provider)
        confirm.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddRate(lang.appLanguage,"Bearer"+user.userData.token,id,rating.rating)
                    ?.enqueue(object : Callback<BaseResponse> {
                        override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@RateActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }


    }
}