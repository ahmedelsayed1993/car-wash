package com.aait.carwash.UI.Activities.Main.Provider

import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.MyServicesModel
import com.aait.carwash.Models.MyServicesResponse
import com.aait.carwash.Models.RateModel
import com.aait.carwash.Models.RatesResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.MyServicesAdapter
import com.aait.carwash.UI.Controllers.RateAdapter
import com.aait.carwash.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RatesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_rates
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var rating:RatingBar
//    lateinit var rv_recycle: RecyclerView
//    internal var layNoInternet: RelativeLayout? = null
//
//    internal var layNoItem: RelativeLayout? = null
//
//    internal var tvNoContent: TextView? = null
//
//    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var myServicesAdapter: RateAdapter
    var Services = ArrayList<RateModel>()

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
//        rv_recycle = findViewById(R.id.rv_recycle)
//        layNoInternet = findViewById(R.id.lay_no_internet)
//        layNoItem = findViewById(R.id.lay_no_item)
//        tvNoContent = findViewById(R.id.tv_no_content)
//        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { onBackPressed()
            finish()}

        title.text = getString(R.string.my_review)
//        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
//        myServicesAdapter = RateAdapter(mContext,Services,R.layout.recycle_rate)
//        rv_recycle.layoutManager = linearLayoutManager
//        rv_recycle.adapter = myServicesAdapter
//        swipeRefresh!!.setColorSchemeResources(
//                R.color.colorPrimary,
//                R.color.colorPrimaryDark,
//                R.color.colorAccent
//        )
//        swipeRefresh!!.setOnRefreshListener {
//            getData()
//
//        }

        getData()

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
//        layNoInternet!!.visibility = View.GONE
//        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Reviews("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object:
                Callback<RatesResponse> {
            override fun onFailure(call: Call<RatesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
//                layNoInternet!!.visibility = View.VISIBLE
//                layNoItem!!.visibility = View.GONE
//                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<RatesResponse>,
                    response: Response<RatesResponse>
            ) {
                hideProgressDialog()
//                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.info?.image).into(image)
                        name.text = response.body()?.data?.info?.name
                        rating.rating = response.body()?.data?.info?.review!!
//                        if (response.body()!!.data?.reviews?.isEmpty()!!) {
//                            layNoItem!!.visibility = View.VISIBLE
//                            layNoInternet!!.visibility = View.GONE
//                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
//
//                        } else {
////
//                            myServicesAdapter.updateAll(response.body()?.data?.reviews!!)
//                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}