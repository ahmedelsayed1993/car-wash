package com.aait.carwash.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.QuestionModel
import com.aait.carwash.Models.QuestionsResponse
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R

import com.aait.carwash.UI.Controllers.QuestionsAdapter
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuestionActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_questions
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var questions: RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var questionsAdapter: QuestionsAdapter
    var QuestionModels=ArrayList<QuestionModel>()
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)

//        notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java)) }
        questions = findViewById(R.id.questions)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        questionsAdapter = QuestionsAdapter(mContext,QuestionModels,R.layout.recycle_question)
        questions.layoutManager = linearLayoutManager
        questions.adapter = questionsAdapter
        back.setOnClickListener { onBackPressed()
            finish() }

        title.text = getString(R.string.repeated_questions)
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Questions(lang.appLanguage)?.enqueue(object:
                Callback<QuestionsResponse> {
            override fun onFailure(call: Call<QuestionsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<QuestionsResponse>, response: Response<QuestionsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        questionsAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}