package com.aait.carwash.UI.Activities.Auth

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.AsyncTask
import android.os.AsyncTask.execute
import android.os.Build
import android.os.Environment
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.net.toFile
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Models.UserResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.AppInfo.TermsActivity
import com.aait.carwash.UI.Activities.LocationActivity
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.URLEncoder
import java.util.*


class ProviderRegisterActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register_provider
    lateinit var back:ImageView
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var manager:EditText
    lateinit var owner:EditText
    lateinit var email:EditText
    lateinit var location:TextView
    lateinit var ID:TextView
    lateinit var store_image:TextView
    lateinit var license:TextView
    lateinit var commercial:EditText
    lateinit var bank_account:EditText
    lateinit var bank_name:EditText
    lateinit var iban:EditText
    lateinit var time_from:TextView
    lateinit var time_to:TextView
    lateinit var saturday: CheckBox
    lateinit var sunday: CheckBox
    lateinit var monday: CheckBox
    lateinit var file:TextView
    lateinit var tuesday: CheckBox
    lateinit var wednesday: CheckBox
    lateinit var thursday: CheckBox
    lateinit var friday: CheckBox
    lateinit var password: EditText
    lateinit var confirm_password: EditText
    lateinit var terms: CheckBox
    lateinit var login: LinearLayout
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var register: Button

    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null

    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath1: String? = null

    internal var returnValue2: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options2 = Options.init()
            .setRequestCode(300)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath2: String? = null

    internal var returnValue3: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options3 = Options.init()
            .setRequestCode(400)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue3)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath3: String? = null
    var deviceID = ""
    var days = ArrayList<String>()
    lateinit var contract:TextView
    var text = ""
    lateinit var SelectedFile:String
    private val FILE_REQUEST_CODE = 666
     var files = ArrayList<MediaFile>()
    var realpath = ""
    var FilePart: MultipartBody.Part? = null
    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })
        back = findViewById(R.id.back)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        manager = findViewById(R.id.manger)
        owner = findViewById(R.id.owner)
        email = findViewById(R.id.email)
        location = findViewById(R.id.location)
        ID = findViewById(R.id.ID)
        store_image = findViewById(R.id.store_image)
        license = findViewById(R.id.license)
        commercial = findViewById(R.id.commercial)
        bank_account = findViewById(R.id.bank_account)
        bank_name = findViewById(R.id.bank_name)
        iban = findViewById(R.id.iban)
        time_from = findViewById(R.id.time_from)
        time_to = findViewById(R.id.time_to)
        saturday = findViewById(R.id.saturday)
        sunday = findViewById(R.id.sunday)
        monday = findViewById(R.id.monday)
        tuesday = findViewById(R.id.tuesday)
        wednesday = findViewById(R.id.wednesday)
        thursday = findViewById(R.id.thursday)
        friday = findViewById(R.id.friday)
        password = findViewById(R.id.password)
        register = findViewById(R.id.register)
        terms = findViewById(R.id.terms)
        confirm_password = findViewById(R.id.confirm_password)
        contract = findViewById(R.id.contract)
        login = findViewById(R.id.login)
        file = findViewById(R.id.file)
        login.setOnClickListener {val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra("type", "provider")
            startActivity(intent)
            finish()}
        terms.setOnClickListener { startActivity(Intent(this, TermsActivity::class.java)) }

        location.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java), 1)
        }
//        showProgressDialog(getString(R.string.please_wait))
//        Client.getClient()?.create(Service::class.java)?.Contract()?.enqueue(object :Callback<TermsResponse>{
//            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
//                hideProgressDialog()
//                if (response.isSuccessful){
//                    if (response.body()?.value.equals("1")){
//                        text = response.body()?.data!!
//                    }else{
//                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
//                    }
//                }
//            }
//
//            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
//                hideProgressDialog()
//                CommonUtil.handleException(mContext,t)
//                t.printStackTrace()
//            }
//
//        })
//        contract.setOnClickListener {
//
//            val i = Intent(Intent.ACTION_VIEW)
//            i.data = Uri.parse(text)
//            startActivity(i)
//
//
//        }
        back.setOnClickListener { onBackPressed()
        finish()}
//        file.setOnClickListener {
////            var intent = Intent(this, FilePickerActivity::class.java)
////            intent.putExtra(
////                    FilePickerActivity.CONFIGS,  Configurations.Builder()
////                    .setCheckPermission(true)
////                    .setShowFiles(true)
////                    .setShowAudios(false)
////                    .setShowVideos(false)
////                    .setShowImages(false)
////                    .setSuffixes("pdf")
////                    .setSingleChoiceMode(true)
////                    .build())
////            startActivityForResult(intent, FILE_REQUEST_CODE)
//            val intent = Intent()
//            intent.action = Intent.ACTION_GET_CONTENT
//            intent.type = "application/pdf"
//            startActivityForResult(intent,205)
//        }

        saturday.setOnClickListener {
            if (saturday.isChecked){
                days.add("saturday")
            }else{
                if (days.contains("saturday")){
                    days.remove("saturday")
                }else{

                }
            }
        }
        sunday.setOnClickListener {
            if (sunday.isChecked){
                days.add("sunday")
            }else{
                if (days.contains("sunday")){
                    days.remove("sunday")
                }else{

                }
            }
        }
        monday.setOnClickListener {
            if (monday.isChecked){
                days.add("monday")
            }else{
                if (days.contains("monday")){
                    days.remove("monday")
                }else{

                }
            }
        }
        tuesday.setOnClickListener {
            if (tuesday.isChecked){
                days.add("tuesday")
            }else{
                if (days.contains("tuesday")){
                    days.remove("tuesday")
                }else{

                }
            }
        }
        wednesday.setOnClickListener {
            if (wednesday.isChecked){
                days.add("wednesday")
            }else{
                if (days.contains("wednesday")){
                    days.remove("wednesday")
                }else{

                }
            }
        }
        thursday.setOnClickListener {
            if (thursday.isChecked){
                days.add("thursday")
            }else{
                if (days.contains("thursday")){
                    days.remove("thursday")
                }else{

                }

            }
        }
        friday.setOnClickListener {
            if (friday.isChecked){
                days.add("friday")
            }else{
                if (days.contains("friday")){
                    days.remove("friday")
                }else{

                }
            }
        }
        time_from.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)
            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)
                            var hour = hourOfDay
                            var minute = minute
                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time_from.text = hours.toString()+":"+minutes.toString()
                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        time_to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)
            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)
                            var hour = hourOfDay
                            var minute = minute
                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time_to.text = hours.toString()+":"+minutes.toString()
                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.to))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        ID.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        store_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        license.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }



        register.setOnClickListener {
            if (CommonUtil.checkEditError(phone, getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone, getString(R.string.phone_length), 9)||
                    CommonUtil.checkEditError(name, getString(R.string.service_provider_name))||
                    CommonUtil.checkEditError(email, getString(R.string.email))||
                    !CommonUtil.isEmailValid(email, getString(R.string.correct_email))||
                    CommonUtil.checkTextError(location, getString(R.string.location))||
                    CommonUtil.checkTextError(ID, getString(R.string.ID))||
                    CommonUtil.checkEditError(bank_account, getString(R.string.bank_account))||
                    CommonUtil.checkEditError(bank_name, getString(R.string.bank_name))||
                    CommonUtil.checkEditError(iban, getString(R.string.iban_number))||
//                    CommonUtil.checkTextError(file,getString(R.string.attach_file_downloaded))||
                    CommonUtil.checkTextError(time_from,getString(R.string.from))||
                    CommonUtil.checkTextError(time_to,getString(R.string.to))){
                return@setOnClickListener
            }else{
                if (days.isEmpty()){
                    CommonUtil.makeToast(mContext,getString(R.string.work_days))
                }else {
                    if (CommonUtil.checkEditError(password, getString(R.string.password)) ||
                            CommonUtil.checkLength(password, getString(R.string.password_length), 6) ||
                            CommonUtil.checkEditError(confirm_password, getString(R.string.confirm_password))) {
                        return@setOnClickListener
                    } else {
                        if (!password.text.toString().equals(confirm_password.text.toString())) {
                            CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                        } else {
                            if (!terms.isChecked) {
                                CommonUtil.makeToast(mContext, getString(R.string.read_accept))
                            } else {


                                        if (ImageBasePath1 != null && ImageBasePath2 != null) {
                                            Register(ImageBasePath!!, ImageBasePath1!!, ImageBasePath2!!, time_from.text.toString(), time_to.text.toString(), days.joinToString(separator = ",", postfix = "", prefix = ""))
                                        } else if (ImageBasePath1 != null && ImageBasePath2 == null) {
                                            Register(ImageBasePath!!, ImageBasePath1!!, time_from.text.toString(), time_to.text.toString(), days.joinToString(separator = ",", postfix = "", prefix = ""))
                                        } else if (ImageBasePath1 == null && ImageBasePath2 != null) {
                                            Registerwith(ImageBasePath!!, ImageBasePath2!!, time_from.text.toString(), time_to.text.toString(), days.joinToString(separator = ",", postfix = "", prefix = ""))
                                        } else {
                                            Register(ImageBasePath!!, time_from.text.toString(), time_to.text.toString(), days.joinToString(separator = ",", postfix = "", prefix = ""))
                                        }


                            }

                        }
                    }
                }
            }
        }


    }

    fun Register(path: String, path1: String, path2: String,start:String?,end:String?,days:String?){



        showProgressDialog(getString(R.string.please_wait))

        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("identification_card", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("shop_photo", ImageFile1.name, fileBody1)
        var filePart2: MultipartBody.Part? = null
        val ImageFile2 = File(path2)
        val fileBody2 = RequestBody.create(MediaType.parse("*/*"), ImageFile2)
        filePart2 = MultipartBody.Part.createFormData("shop_license", ImageFile2.name, fileBody2)

        Client.getClient()?.create(Service::class.java)?.SignUpProvider(lang.appLanguage, phone.text.toString(), name.text.toString(),  email.text.toString(), lat, lng, location.text.toString(), filePart, filePart1, filePart2, commercial.text.toString(), bank_account.text.toString(), bank_name.text.toString(), iban.text.toString(), start, end, days, password.text.toString(), deviceID, "android"
        )?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext, t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        user.loginStatus = false
                        val intent = Intent(this@ProviderRegisterActivity, ActivateAccountActivity::class.java)
                        intent.putExtra("user", response.body()?.data)
                        startActivity(intent)
                        finish()
                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun Register(path: String,start:String?,end:String?,days:String?){
        showProgressDialog(getString(R.string.please_wait))

        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("identification_card", ImageFile.name, fileBody)


        Client.getClient()?.create(Service::class.java)?.SignUpProviderwithout(lang.appLanguage, phone.text.toString(), name.text.toString(),  email.text.toString(), lat, lng, location.text.toString(), filePart,  commercial.text.toString(), bank_account.text.toString(), bank_name.text.toString(), iban.text.toString(), start, end, days, password.text.toString(), deviceID, "android"
        )?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext, t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        user.loginStatus = false
                        val intent = Intent(this@ProviderRegisterActivity, ActivateAccountActivity::class.java)
                        intent.putExtra("user", response.body()?.data)
                        startActivity(intent)
                        finish()
                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun Registerwith(path: String, path2: String,start:String?,end:String?,days:String?){
        showProgressDialog(getString(R.string.please_wait))

        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("identification_card", ImageFile.name, fileBody)

        var filePart2: MultipartBody.Part? = null
        val ImageFile2 = File(path2)
        val fileBody2 = RequestBody.create(MediaType.parse("*/*"), ImageFile2)
        filePart2 = MultipartBody.Part.createFormData("shop_license", ImageFile2.name, fileBody2)

        Client.getClient()?.create(Service::class.java)?.SignUpProviderwithLie(lang.appLanguage, phone.text.toString(), name.text.toString(),  email.text.toString(), lat, lng, location.text.toString(), filePart, filePart2, commercial.text.toString(), bank_account.text.toString(), bank_name.text.toString(), iban.text.toString(), start, end, days, password.text.toString(), deviceID, "android"
        )?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext, t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        user.loginStatus = false
                        val intent = Intent(this@ProviderRegisterActivity, ActivateAccountActivity::class.java)
                        intent.putExtra("user", response.body()?.data)
                        startActivity(intent)
                        finish()
                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun Register(path: String, path1: String,start:String?,end:String?,days:String?){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("identification_card", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("shop_photo", ImageFile1.name, fileBody1)


        Client.getClient()?.create(Service::class.java)?.SignUpProviderwithpoto(lang.appLanguage, phone.text.toString(), name.text.toString(),  email.text.toString(), lat, lng, location.text.toString(), filePart, filePart1, commercial.text.toString(), bank_account.text.toString(), bank_name.text.toString(), iban.text.toString(), start, end, days, password.text.toString(), deviceID, "android"
        )?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext, t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        user.loginStatus = false
                        val intent = Intent(this@ProviderRegisterActivity, ActivateAccountActivity::class.java)
                        intent.putExtra("user", response.body()?.data)
                        startActivity(intent)
                        finish()
                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }else{
            if (requestCode == 100) {
                if (resultCode == 0) {

                } else {
                    returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath = returnValue!![0]



                    if (ImageBasePath != null) {

                        ID.text  = getString(R.string.image_attached)
                    }
                }
            }else if (requestCode == 200) {
                if (resultCode == 0) {

                } else {
                    returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath1 = returnValue1!![0]



                    if (ImageBasePath1 != null) {

                        store_image.text  = getString(R.string.image_attached)
                    }
                }
            }else if (requestCode == 300) {
                if (resultCode == 0) {

                } else {
                    returnValue2 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath2 = returnValue2!![0]



                    if (ImageBasePath2 != null) {

                        license.text  = getString(R.string.image_attached)
                    }
                }
            }else {
//                if (requestCode == 205 && data != null){
//                    val uri = data.data
//                    realpath = saveFileInStorage(mContext,uri!!)
//                    Log.e("saveFileInStorage",realpath )
//                    val files = File(realpath)
//                    val requestBody = RequestBody.create(MediaType.parse("/"), files)
//                    FilePart = MultipartBody.Part.createFormData("contract", URLEncoder.encode(files.name, "utf-8"), requestBody)
//                    file.text = getString(R.string.file_selected)
//                }
            }
        }
    }

    fun saveFileInStorage(mContext: Context, uri: Uri): String {
        var path = ""
//        Thread {
        var file: File? = null
        try {
            val mimeType: String? = mContext.contentResolver.getType(uri)
            if (mimeType != null) {
                val inputStream: InputStream? = mContext.contentResolver.openInputStream(uri)
                val fileName = getFileName(mContext,uri)
                if (fileName != "") {
                    file = File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.getAbsolutePath().toString() + "/" + fileName)
                    val output: OutputStream = FileOutputStream(file)
                    try {
                        val buffer =
                                ByteArray(inputStream?.available()!!) // or other buffer size
                        var read: Int = 0
                        while (inputStream.read(buffer).also({ read = it }) != -1) {
                            output.write(buffer, 0, read)
                        }
                        output.flush()
                        path = file.getAbsolutePath() //use this path
                    } finally {
                        output.close()
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
//        }.start()
        return path
    }
    @SuppressLint("Range")
    fun getFileName(mContext: Context, uri: Uri): String {
        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        var displayName = ""
        var cursor: Cursor? = null
        cursor = mContext.contentResolver.query(uri, null, null, null, null, null)
        try {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                )
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {

            cursor?.close()
        }
        return displayName
    }
}
//private class DownloadFile : AsyncTask<String?, Void?, Void?>() {
//
//
//    override fun doInBackground(vararg p0: String?): Void? {
//        val fileUrl = p0[0] // -> http://maven.apache.org/maven-1.x/maven.pdf
//        val fileName = p0[0] // -> maven.pdf
//        val extStorageDirectory: String = Environment.getExternalStorageDirectory().toString()
//        val folder = File(extStorageDirectory, "testthreepdf")
//        folder.mkdir()
//        val pdfFile = File(folder, fileName)
//        try {
//            pdfFile.createNewFile()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//        FileDownloader.downloadFile(fileUrl, pdfFile)
//        return null
//    }
//}
//object FileDownloader {
//    private const val MEGABYTE = 1024 * 1024
//    fun downloadFile(fileUrl: String?, directory: File?) {
//        try {
//            val url = URL(fileUrl)
//            val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
//            //urlConnection.setRequestMethod("GET");
//            //urlConnection.setDoOutput(true);
//            urlConnection.connect()
//            val inputStream: InputStream = urlConnection.getInputStream()
//            val fileOutputStream = FileOutputStream(directory)
//            val totalSize: Int = urlConnection.getContentLength()
//            val buffer = ByteArray(MEGABYTE)
//            var bufferLength = 0
//            while (inputStream.read(buffer).also { bufferLength = it } > 0) {
//                fileOutputStream.write(buffer, 0, bufferLength)
//            }
//            fileOutputStream.close()
//        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
//        } catch (e: MalformedURLException) {
//            e.printStackTrace()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//    }
//}

