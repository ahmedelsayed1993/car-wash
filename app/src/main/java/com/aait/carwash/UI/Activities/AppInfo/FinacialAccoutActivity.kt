package com.aait.carwash.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.FinacialAccountModel
import com.aait.carwash.Models.FincialModel
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Client.BanksActivity

import com.aait.carwash.UI.Controllers.FinancialAdapter
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FinacialAccoutActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_finatial
    lateinit var back:ImageView
    lateinit var title:TextView

    lateinit var accounts:RecyclerView
    lateinit var total:TextView
    lateinit var Indebtedness:TextView
    lateinit var settlment:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var financialAdapter: FinancialAdapter
    var finacials = ArrayList<FincialModel>()
    var price = ""
    lateinit var tax:TextView
    lateinit var final_total:TextView
    lateinit var added:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        tax = findViewById(R.id.tax)
        final_total = findViewById(R.id.final_total)
        accounts = findViewById(R.id.accounts)
        total = findViewById(R.id.total)
        Indebtedness = findViewById(R.id.Indebtedness)
        settlment = findViewById(R.id.settlment)
        added = findViewById(R.id.added)
        linearLayoutManager  = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        financialAdapter = FinancialAdapter(mContext,finacials,R.layout.recycle_finacial)
        accounts.layoutManager = linearLayoutManager
        accounts.adapter = financialAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Financial_accounts)

        getData()
        settlment.setOnClickListener {
            val intent = Intent(this, BankActivity::class.java)
            intent.putExtra("price",price)
            startActivity(intent)
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Finacial("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object : Callback<FinacialAccountModel>{
            override fun onFailure(call: Call<FinacialAccountModel>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<FinacialAccountModel>, response: Response<FinacialAccountModel>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        financialAdapter.updateAll(response.body()?.data!!)
                        total.text = response.body()?.total+getString(R.string.rs)
                        Indebtedness.text = response.body()?.indebtedness.toString()+getString(R.string.rs)
                        tax.text = getString(R.string.value_added_tax)+"("+response.body()?.tax+"%)"
                         added.text = response.body()?.financial_tax+getString(R.string.rs)
                        final_total.text = response.body()?.total_payment+getString(R.string.rs)
                        price = response.body()?.total_payment.toString()
                        if (response.body()?.total_payment.equals("0")){
                            settlment.visibility = View.GONE
                        }else{
                            settlment.visibility = View.VISIBLE
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}