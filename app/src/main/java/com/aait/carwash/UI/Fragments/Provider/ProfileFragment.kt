package com.aait.carwash.UI.Fragments.Provider

import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.aait.carwash.Base.BaseFragment
import com.aait.carwash.Models.UserModel
import com.aait.carwash.Models.UserResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Auth.ChangePasswordActivity
import com.aait.carwash.UI.Activities.Auth.ProviderRegisterActivity
import com.aait.carwash.UI.Activities.LocationActivity
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*


class ProfileFragment : BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_provider_profile
    companion object {
        fun newInstance(): ProfileFragment {
            val args = Bundle()
            val fragment = ProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var image:CircleImageView
    lateinit var save:Button
    lateinit var change_pass:TextView
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var manager: EditText
    lateinit var owner: EditText
    lateinit var email: EditText
    lateinit var location:TextView
    lateinit var ID:TextView
    lateinit var store_image:TextView
    lateinit var license:TextView
    lateinit var commercial: EditText
//    lateinit var time_from:TextView
//    lateinit var time_to:TextView
//    lateinit var saturday: CheckBox
//    lateinit var sunday: CheckBox
//    lateinit var monday: CheckBox
//    lateinit var tuesday: CheckBox
//    lateinit var wednesday: CheckBox
//    lateinit var thursday: CheckBox
//    lateinit var friday: CheckBox
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null

    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath1: String? = null

    internal var returnValue2: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options2 = Options.init()
            .setRequestCode(300)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath2: String? = null

    internal var returnValue3: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options3 = Options.init()
            .setRequestCode(400)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue3)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath3: String? = null
    var deviceID = ""
    var days = ArrayList<String>()
    var lat = ""
    var lng = ""
    var result = ""

    override fun initializeComponents(view: View) {
        image = view.findViewById(R.id.image)
        save = view.findViewById(R.id.save)
        change_pass = view.findViewById(R.id.change_pass)
        phone = view.findViewById(R.id.phone)
        name = view.findViewById(R.id.name)
        manager = view.findViewById(R.id.manger)
        owner = view.findViewById(R.id.owner)
        email = view.findViewById(R.id.email)
        location = view.findViewById(R.id.location)
        ID = view.findViewById(R.id.ID)
        store_image = view.findViewById(R.id.store_image)
        license = view.findViewById(R.id.license)
        commercial = view.findViewById(R.id.commercial)
//        time_from = view.findViewById(R.id.time_from)
//        time_to = view.findViewById(R.id.time_to)
//        saturday = view.findViewById(R.id.saturday)
//        sunday = view.findViewById(R.id.sunday)
//        monday = view.findViewById(R.id.monday)
//        tuesday = view.findViewById(R.id.tuesday)
//        wednesday = view.findViewById(R.id.wednesday)
//        thursday = view.findViewById(R.id.thursday)
//        friday = view.findViewById(R.id.friday)
        location.setOnClickListener {
            startActivityForResult(Intent(activity, LocationActivity::class.java),1)
        }

//        saturday.setOnClickListener {
//            if (saturday.isChecked){
//                days.add("saturday")
//            }else{
//                if (days.contains("saturday")){
//                    days.remove("saturday")
//                }else{
//
//                }
//            }
//        }
//        sunday.setOnClickListener {
//            if (sunday.isChecked){
//                days.add("sunday")
//            }else{
//                if (days.contains("sunday")){
//                    days.remove("sunday")
//                }else{
//
//                }
//            }
//        }
//        monday.setOnClickListener {
//            if (monday.isChecked){
//                days.add("monday")
//            }else{
//                if (days.contains("monday")){
//                    days.remove("monday")
//                }else{
//
//                }
//            }
//        }
//        tuesday.setOnClickListener {
//            if (tuesday.isChecked){
//                days.add("tuesday")
//            }else{
//                if (days.contains("tuesday")){
//                    days.remove("tuesday")
//                }else{
//
//                }
//            }
//        }
//        wednesday.setOnClickListener {
//            if (wednesday.isChecked){
//                days.add("wednesday")
//            }else{
//                if (days.contains("wednesday")){
//                    days.remove("wednesday")
//                }else{
//
//                }
//            }
//        }
//        thursday.setOnClickListener {
//            if (thursday.isChecked){
//                days.add("thursday")
//            }else{
//                if (days.contains("thursday")){
//                    days.remove("thursday")
//                }else{
//
//                }
//
//            }
//        }
//        friday.setOnClickListener {
//            if (friday.isChecked){
//                days.add("friday")
//            }else{
//                if (days.contains("friday")){
//                    days.remove("friday")
//                }else{
//
//                }
//            }
//        }
//        time_from.setOnClickListener {
//            val myCalender = Calendar.getInstance()
//            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
//            val minute = myCalender.get(Calendar.MINUTE)
//            val myTimeListener =
//                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
//                        if (view.isShown) {
//                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
//                            myCalender.set(Calendar.MINUTE, minute)
//                            var hour = hourOfDay
//                            var minute = minute
//                            val hours = if (hour < 10) "0" + hour else hour
//                            val minutes = if (minute < 10) "0" + minute else minute
//                            time_from.text = hours.toString()+":"+minutes.toString()
//                        }
//                    }
//            val timePickerDialog = TimePickerDialog(
//                    mContext!!,
//                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
//                    myTimeListener,
//                    hour,
//                    minute,
//                    false
//            )
//            timePickerDialog.setTitle(getString(R.string.from))
//            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)
//
//            timePickerDialog.show()
//        }
//        time_to.setOnClickListener {
//            val myCalender = Calendar.getInstance()
//            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
//            val minute = myCalender.get(Calendar.MINUTE)
//            val myTimeListener =
//                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
//                        if (view.isShown) {
//                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
//                            myCalender.set(Calendar.MINUTE, minute)
//                            var hour = hourOfDay
//                            var minute = minute
//                            val hours = if (hour < 10) "0" + hour else hour
//                            val minutes = if (minute < 10) "0" + minute else minute
//                            time_to.text = hours.toString()+":"+minutes.toString()
//                        }
//                    }
//            val timePickerDialog = TimePickerDialog(
//                    mContext!!,
//                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
//                    myTimeListener,
//                    hour,
//                    minute,
//                    false
//            )
//            timePickerDialog.setTitle(getString(R.string.to))
//            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)
//
//            timePickerDialog.show()
//        }
        ID.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        store_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        license.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options3)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options3)
            }
        }
        change_pass.setOnClickListener { startActivity(Intent(activity,ChangePasswordActivity::class.java)) }
       getData()
        save.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(name,getString(R.string.service_provider_name))||
                    CommonUtil.checkEditError(manager,getString(R.string.Name_of_the_store_manager))||
                    CommonUtil.checkEditError(owner,getString(R.string.owner_name))||
                    CommonUtil.checkEditError(email,getString(R.string.email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(location,getString(R.string.location))||
                    CommonUtil.checkEditError(commercial,getString(R.string.Commercial_Register))){
                return@setOnClickListener
            }else{

                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.ProviderProfile("Bearer"+user.userData.token,lang.appLanguage,phone.text.toString()
                            ,name.text.toString(),manager.text.toString(),owner.text.toString(),email.text.toString(),lat,lng,location.text.toString(),commercial.text.toString(),null,null,null)
                            ?.enqueue(object : Callback<UserResponse> {
                                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                                    hideProgressDialog()
                                    CommonUtil.handleException(mContext!!,t)
                                    t.printStackTrace()
                                }

                                override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                            user.userData = response.body()?.data!!
                                            Log.e("user",Gson().toJson(response.body()
                                                    ?.data))
                                            SetData(response.body()?.data!!)
                                        }else{
                                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                                        }
                                    }
                                }

                            })

            }
        }

    }
    fun SetData(userModel: UserModel){
        Glide.with(mContext!!).asBitmap().load(userModel.avatar).into(image)
        phone.setText(userModel.phone)
        name.setText(userModel.name)
        manager.setText(userModel.manger_name)
        owner.setText(userModel.owner_name)
        email.setText(userModel.email)
        location.text = userModel.address
        lat = userModel.lat!!
        lng = userModel.lng!!
        result = userModel.address!!
        if (!userModel.identification_card.equals("")){
            ID.text = mContext!!.getString(R.string.image_attached)
        }else{
            ID.text = ""
        }
        if (!userModel.shop_license.equals("")){
            license.text = mContext!!.getString(R.string.image_attached)
        }else{
            license.text = ""
        }
        if (!userModel.shop_photo.equals("")){
            store_image.text = mContext!!.getString(R.string.image_attached)
        }else{
            store_image.text = ""
        }
        commercial.setText(userModel.commercial)
//        time_from.text = userModel.time_from
//        time_to.text = userModel.time_to
//        days = userModel.days!!
//        if (days.contains("saturday")){
//            saturday.isChecked = true
//        }else{
//            saturday.isChecked = false
//        }
//        if (days.contains("sunday")){
//            sunday.isChecked = true
//        }else{
//            sunday.isChecked = false
//        }
//        if (days.contains("monday")){
//            monday.isChecked = true
//        }else{
//            monday.isChecked = false
//        }
//        if (days.contains("tuesday")){
//            tuesday.isChecked = true
//        }else{
//            tuesday.isChecked = false
//        }
//        if (days.contains("wednesday")){
//            wednesday.isChecked = true
//        }else{
//            wednesday.isChecked = false
//        }
//        if (days.contains("thursday")){
//            thursday.isChecked = true
//        }else{
//            thursday.isChecked = false
//        }
//        if (days.contains("friday")){
//            friday.isChecked = true
//        }else{
//            friday.isChecked = false
//        }


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderProfile("Bearer"+user.userData.token,lang.appLanguage,null
        ,null,null,null,null,null,null,null,null,null,null,null)
                ?.enqueue(object : Callback<UserResponse> {
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                user.userData = response.body()?.data!!
                                Log.e("user",Gson().toJson(response.body()
                                        ?.data))
                                SetData(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun Avatar(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.ProviderAvatar("Bearer"+user.userData.token,lang.appLanguage,filePart)
                ?.enqueue(object :Callback<UserResponse>{
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                user.userData = response.body()?.data!!
                                Log.e("user",Gson().toJson(response.body()
                                        ?.data))
                                SetData(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun ID(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("identification_card", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.ProviderID("Bearer"+user.userData.token,lang.appLanguage,filePart)
                ?.enqueue(object :Callback<UserResponse>{
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                user.userData = response.body()?.data!!
                                Log.e("user",Gson().toJson(response.body()
                                        ?.data))
                                SetData(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun Licences(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("shop_license", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.ProviderLiece("Bearer"+user.userData.token,lang.appLanguage,filePart)
                ?.enqueue(object :Callback<UserResponse>{
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                user.userData = response.body()?.data!!
                                Log.e("user",Gson().toJson(response.body()
                                        ?.data))
                                SetData(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun Shop(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("shop_photo", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.ProviderShop("Bearer"+user.userData.token,lang.appLanguage,filePart)
                ?.enqueue(object :Callback<UserResponse>{
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                user.userData = response.body()?.data!!
                                Log.e("user",Gson().toJson(response.body()
                                        ?.data))
                                SetData(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }else{
            if (requestCode == 100) {
                if (resultCode == 0) {

                } else {
                    returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath = returnValue!![0]



                    if (ImageBasePath != null) {

                        ID.text  = getString(R.string.image_attached)
                        ID(ImageBasePath!!)
                    }
                }
            }else if (requestCode == 200) {
                if (resultCode == 0) {

                } else {
                    returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath1 = returnValue1!![0]



                    if (ImageBasePath1 != null) {

                        store_image.text  = getString(R.string.image_attached)
                        Shop(ImageBasePath1!!)
                    }
                }
            }else if (requestCode == 300) {
                if (resultCode == 0) {

                } else {
                    returnValue2 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath2 = returnValue2!![0]



                    if (ImageBasePath2 != null) {

                        license.text  = getString(R.string.image_attached)
                        Licences(ImageBasePath2!!)
                    }
                }
            }else if (requestCode == 400) {
                if (resultCode == 0) {

                } else {
                    returnValue3 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath3 = returnValue3!![0]



                    if (ImageBasePath3 != null) {

                        Glide.with(mContext!!).load(ImageBasePath3).into(image)
                        Avatar(ImageBasePath3!!)
                    }
                }
            }
        }
    }
}