package com.aait.carwash.UI.Activities.Main.Provider

import android.content.Intent
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.CheckOfferResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.AppInfo.*
import com.aait.carwash.UI.Activities.Auth.LoginActivity
import com.aait.carwash.UI.Activities.Main.Client.NotificationActivity
import com.aait.carwash.UI.Activities.Main.Client.ServicesActivity
import com.aait.carwash.UI.Activities.SplashActivity
import com.aait.carwash.UI.Fragments.Provider.HomeFragment
import com.aait.carwash.UI.Fragments.Provider.OrdersFragment
import com.aait.carwash.UI.Fragments.Provider.ProfileFragment
import com.aait.carwash.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.infideap.drawerbehavior.AdvanceDrawerLayout
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.R.id

import android.os.Bundle




class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_main
    lateinit var menu: ImageView
    lateinit var notification: ImageView
    lateinit var drawer_layout: AdvanceDrawerLayout
    lateinit var main:LinearLayout
    lateinit var mydata:LinearLayout
    lateinit var myorders:LinearLayout
    lateinit var services:LinearLayout
    lateinit var offers:LinearLayout
    lateinit var package_orders:LinearLayout
    lateinit var my_review:LinearLayout
    lateinit var about_app:LinearLayout
    lateinit var questions:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var contact_us:LinearLayout
    lateinit var complaints:LinearLayout
    lateinit var settings:LinearLayout
    lateinit var logout:LinearLayout
    lateinit var home: ImageView
    lateinit var orders: ImageView
    lateinit var profile: ImageView
    lateinit var finacial:LinearLayout
    private var fragmentManager: FragmentManager? = null
    lateinit var bottom: RelativeLayout
    var selected = 1
    lateinit var image:CircleImageView
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var ordersFragment: OrdersFragment
    internal lateinit var profileFragment: ProfileFragment
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun initializeComponents() {
        firebaseAnalytics = Firebase.analytics
        firebaseAnalytics.logEvent("ProviderHome",null)
//        {
//            param(FirebaseAnalytics.Param.ITEM_ID, id)
//            param(FirebaseAnalytics.Param.ITEM_NAME, name)
//            param(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
//        }
        drawer_layout = findViewById(R.id.drawer_layout)
        menu = findViewById(R.id.menu)
        notification = findViewById(R.id.notification)
        home = findViewById(R.id.home)
        orders = findViewById(R.id.orders)
        profile = findViewById(R.id.profile)
        bottom = findViewById(R.id.bottom)
        homeFragment = HomeFragment.newInstance()
        ordersFragment = OrdersFragment.newInstance()
        profileFragment = ProfileFragment.newInstance()
        fragmentManager = supportFragmentManager
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container,homeFragment)
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,profileFragment)
        transaction!!.commit()
        showhome()
        notification.setOnClickListener {
            startActivity(Intent(this,NotificationActivity::class.java))
            finish()
        }
        home.setOnClickListener { showhome() }
        orders.setOnClickListener { showOrders() }
        profile.setOnClickListener { showprofile() }
        sideMenu()
    }

    fun showhome(){
        selected = 1
        home.setImageResource(R.mipmap.home_active)
        orders.setImageResource(R.mipmap.terms)
        profile.setImageResource(R.mipmap.profile)
        bottom.setBackgroundResource(R.mipmap.navegation_bar)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }
    fun showOrders(){
        if (user.loginStatus!!) {
            selected = 1
            home.setImageResource(R.mipmap.home)
            orders.setImageResource(R.mipmap.terms_active)
            profile.setImageResource(R.mipmap.profile)
            bottom.setBackgroundResource(R.mipmap.navegation_bar_two)

            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, ordersFragment)
            transaction!!.commit()
        }else{
            //CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
    }
    fun showprofile(){
        if (user.loginStatus!!){
            selected = 1
            home.setImageResource(R.mipmap.home)
            orders.setImageResource(R.mipmap.terms)
            profile.setImageResource(R.mipmap.profile_active)
            bottom.setBackgroundResource(R.mipmap.navegation_bar_two)

            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, profileFragment)
            transaction!!.commit()
        }else{
            // CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
    }

    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 150f)
        drawer_layout.setRadius(Gravity.END, 150f)
        drawer_layout.setViewScale(Gravity.START, 1f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 1f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 10f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 10f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(2f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
        offers = drawer_layout.findViewById(R.id.offers)
        package_orders = drawer_layout.findViewById(R.id.package_orders)
        image = drawer_layout.findViewById(R.id.image)
        main = drawer_layout.findViewById(R.id.main)
        mydata = drawer_layout.findViewById(R.id.mydata)
        settings = drawer_layout.findViewById(R.id.settings)
        myorders = drawer_layout.findViewById(R.id.myorders)
        Glide.with(mContext).asBitmap().load(user.userData.avatar).into(image)
        terms = drawer_layout.findViewById(R.id.terms)
        about_app = drawer_layout.findViewById(R.id.about_app)
        contact_us = drawer_layout.findViewById(R.id.contact_us)
        complaints = drawer_layout.findViewById(R.id.complaints)
        questions = drawer_layout.findViewById(R.id.questions)
        services = drawer_layout.findViewById(R.id.services)
        logout = drawer_layout.findViewById(R.id.logout)
        finacial = drawer_layout.findViewById(R.id.finacial)
        my_review = drawer_layout.findViewById(R.id.my_review)
        Client.getClient()?.create(Service::class.java)?.CheckOffer()?.enqueue(object :Callback<CheckOfferResponse>{
            override fun onResponse(call: Call<CheckOfferResponse>, response: Response<CheckOfferResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            offers.visibility = View.VISIBLE
                            package_orders.visibility = View.VISIBLE
                        }else{
                            offers.visibility = View.GONE
                            package_orders.visibility = View.GONE
                        }

                    }
                }
            }

            override fun onFailure(call: Call<CheckOfferResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }
        })
        terms.setOnClickListener { startActivity(Intent(this, TermsActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}
        questions.setOnClickListener { startActivity(Intent(this, QuestionActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}
        mydata.setOnClickListener {

            showprofile()
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        finacial.setOnClickListener {startActivity(Intent(this, FinacialAccoutActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)  }
        offers.setOnClickListener { startActivity(Intent(this, MyOffersActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START) }
        package_orders.setOnClickListener { startActivity(Intent(this, OffersOrdersActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START) }
        my_review.setOnClickListener { startActivity(Intent(this,RatesActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}

        myorders.setOnClickListener { showOrders()
            drawer_layout.closeDrawer(GravityCompat.START)}
        services.setOnClickListener {
            startActivity(Intent(this, MyServicesActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        about_app.setOnClickListener { startActivity(Intent(this, AboutAppActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}
        contact_us.setOnClickListener { startActivity(Intent(this,ContactUsActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}

        complaints.setOnClickListener { startActivity(Intent(this, ComplaintActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}
        settings.setOnClickListener { startActivity(Intent(this, ProviderSettingsActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}
        main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)}
        logout.setOnClickListener { logout()
            drawer_layout.closeDrawer(GravityCompat.START)}

    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,lang.appLanguage)?.enqueue(object :
                Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()

                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else if(response.body()?.value.equals("401")){
                        user.loginStatus=false
                        user.Logout()
                        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                        finish()
                    }else{
                        user.loginStatus=false
                        user.Logout()

                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))

                    }
                }
            }
        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}


