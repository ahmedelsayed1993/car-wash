package com.aait.carwash.UI.Activities.Main.Client

import android.Manifest
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Cart.AllCartViewModel
import com.aait.carwash.Cart.CartDataBase
import com.aait.carwash.Cart.ProviderModelOffline
import com.aait.carwash.GPS.GPSTracker
import com.aait.carwash.GPS.GpsTrakerListener
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.LocationActivity
import com.aait.carwash.UI.Controllers.TimesAdapter
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.DialogUtil
import com.aait.carwash.Utils.PermissionUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class CompleteCartOrder  : ParentActivity(), GpsTrakerListener,OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_complete_order
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var current: TextView
    lateinit var night: Switch
    lateinit var location: TextView
    lateinit var text: LinearLayout
    lateinit var name: EditText
    lateinit var phone: EditText
    lateinit var date: TextView
    lateinit var time: RecyclerView
    var timesModels = ArrayList<TimeModel>()
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var timesAdapter:TimesAdapter
    lateinit var next: Button
    lateinit var check: CheckBox
    lateinit var notes: EditText
    var provider_id = 0
    var provider_name=""
    var provider_image= ""
    var address_ = ""
    var type_id = 0
    var name_ = ""
    var service_id = 0
    var count = 0
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()

    var id = 0
    var lat = ""
    var lng = ""
    var result = ""
    var state = 0
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var serviceModel: ServicesModel
    lateinit var providerDetailsModel: ProviderDetailsModel
    lateinit var productModel: ProductModel
    var offer = "0"
    var h = 0
    var h1 = 0
    var timing = ""
    override fun initializeComponents() {
        provider_id = intent.getIntExtra("provider_id",0)
        provider_image = intent.getStringExtra("provider_image")!!
        provider_name = intent.getStringExtra("provider_name")!!
        address_ = intent.getStringExtra("address")!!
        type_id = intent.getIntExtra("type_id",0)
        name_ = intent.getStringExtra("name")!!
        service_id = intent.getIntExtra("service_id",0)
        count = intent.getIntExtra("count",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        current = findViewById(R.id.current)
        night = findViewById(R.id.night)
        location = findViewById(R.id.location)
        text = findViewById(R.id.text)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        next = findViewById(R.id.next)
        check = findViewById(R.id.check)
        notes = findViewById(R.id.notes)
        title.text = getString(R.string.confirm_order)
        phone.setText(user.userData.phone)
        name.setText(user.userData.name)
        getLocationWithPermission()
        Check()
        gridLayoutManager = GridLayoutManager(mContext,5)
        timesAdapter = TimesAdapter(mContext,timesModels,R.layout.recycle_time)
        timesAdapter.setOnItemClickListener(this)
        time.layoutManager = gridLayoutManager
        time.adapter = timesAdapter

        back.setOnClickListener { onBackPressed()
            finish()}

        if (state==0){

            night.isChecked = true
            text.visibility = View.GONE
            location.visibility = View.GONE
            current.text = result
        }else{
            lat = ""
            lng = ""
            result = ""
            night.isChecked = false
            text.visibility = View.VISIBLE
            location.visibility = View.VISIBLE
            current.text = result
        }
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){

                }else {

                    Log.e("cart", Gson().toJson(cartModels))


                }


            }
        })
        night.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                state = 0
                getLocationWithPermission()
                night.isChecked = true
                text.visibility = View.GONE
                location.visibility = View.GONE
                current.text = result
            }else{
                state = 1
                lat = ""
                lng = ""
                result = ""
                night.isChecked = false
                text.visibility = View.VISIBLE
                location.visibility = View.VISIBLE
                current.text = result
            }
        })
        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                if (m<10){
                    if (dayOfMonth<10){
                        date.setText("" + year + "-" +"0"+ m + "-" +"0"+ dayOfMonth)
                    }else{
                        date.setText("" + year + "-" +"0"+ m + "-" + dayOfMonth)
                    }
                }else{
                    if (dayOfMonth<10){
                        date.setText("" + year + "-" + m + "-" +"0"+ dayOfMonth)
                    }else{
                        date.setText("" + year + "-" + m + "-" + dayOfMonth)
                    }
                }
                getTimes(date.text.toString())
//                date.setText("" + year + "-" + m + "-" + dayOfMonth)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

//        time.setOnClickListener {
//            val myCalender = Calendar.getInstance()
//            h = myCalender.get(Calendar.HOUR_OF_DAY)
//            val minute = myCalender.get(Calendar.MINUTE)
//            val myTimeListener =
//                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
//                        if (view.isShown) {
//                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
//                            myCalender.set(Calendar.MINUTE, minute)
//                             h1 = hourOfDay
//                            var minute = minute
//                            // hour1 = hourOfDay.toString()
//                            var am_pm = ""
//                            val hours = if (h1 < 10) "0" + h1 else h1
//                            val minutes = if (minute < 10) "0" + minute else minute
//                            time.text = hours.toString()+":"+minutes.toString()
//                        }
//                    }
//            val timePickerDialog = TimePickerDialog(
//                    this,
//                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
//                    myTimeListener,
//                    h,minute,
//                    false
//            )
//            timePickerDialog.setTitle(getString(R.string.time))
//            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)
//            timePickerDialog.show()
//        }
        location.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        next.setOnClickListener {
            if (state==0){
                if (CommonUtil.checkTextError(current,getString(R.string.your_current_location))||
                        CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                        CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                        CommonUtil.checkTextError(date,getString(R.string.date))
                    //||CommonUtil.checkTextError(time,getString(R.string.time))
                ){
                    return@setOnClickListener
                }else{
                    if (timing.equals("")){
                        CommonUtil.makeToast(mContext,getString(R.string.choose_time))
                    }else {

                        if (check.isChecked) {
                            AddOrder(offer.toInt())
                        } else {
                            AddOrder(null)
                        }
                    }

                }
            }else{
                if (CommonUtil.checkTextError(location,getString(R.string.another_location))||
                        CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                        CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                        CommonUtil.checkTextError(date,getString(R.string.date))
                   // ||CommonUtil.checkTextError(time,getString(R.string.time))
                ){
                    return@setOnClickListener
                }else{
                    if (timing.equals("")){
                        CommonUtil.makeToast(mContext,getString(R.string.choose_time))
                    }else {
                        if (check.isChecked) {
                            AddOrder(offer.toInt())
                        } else {
                            AddOrder(null)
                        }
                    }
                }
            }
        }

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }
    fun Check(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Check("Bearer"+user.userData.token,lang.appLanguage,provider_id!!)
                ?.enqueue(object : Callback<TermsResponse> {
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                check.visibility = View.VISIBLE
                                offer = response.body()?.data!!
                            }else{
                                check.visibility = View.GONE
                            }
                        }
                    }

                })
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                lat = gps.getLatitude().toString()
                lng = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(lat),
                            java.lang.Double.parseDouble(lng),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        current.text = result
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
                    }

                } catch (e: IOException) {
                }
                //putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }
    }

    fun getTimes(date:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getTimes(lang.appLanguage,provider_id!!,date)?.enqueue(object :Callback<TimeResponse>{
            override fun onResponse(call: Call<TimeResponse>, response: Response<TimeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        timesAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<TimeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }

    fun AddOrder(offer:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder("Bearer"+user.userData.token,lang.appLanguage,
                provider_id!!,service_id!!,count!!,type_id!!
                ,lat,lng,result,name.text.toString(),phone.text.toString(),date.text.toString(),timing,notes.text.toString(),offer)
                ?.enqueue(object : Callback<OrderResponse> {
                    override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<OrderResponse>, response: Response<OrderResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                val intent = Intent(this@CompleteCartOrder,InviceActivity::class.java)
                                intent.putExtra("id",response.body()?.data?.order_id)
                                startActivity(intent)
                                finish()
                                allCartViewModel!!.allCart.observe(this@CompleteCartOrder, object : Observer<List<ProviderModelOffline>> {
                                    override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                                        cartModelOfflines = cartModels


                                        for (i in 0..cartModels.size-1){
                                            if (cartModels.get(i).service_id==service_id) {
                                                allCartViewModel!!.deleteItem(cartModels.get(i))

                                            }

                                        }
                                    }
                                })

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.time){
            if (timesModels.get(position).status.equals("booked")){
                timing = ""
                CommonUtil.makeToast(mContext,getString(R.string.time_booked))
            }else if (timesModels.get(position).status.equals("available")){
                timesAdapter.selected = position
                timesModels.get(position).selected = true
                timesAdapter.notifyDataSetChanged()
                timing = timesModels.get(position).time!!
               // CommonUtil.makeToast(mContext,getString(R.string.time_choosed))
            }
        }
    }
}