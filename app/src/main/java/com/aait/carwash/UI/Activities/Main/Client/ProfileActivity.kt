package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.UserResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Auth.ChangePasswordActivity
import com.aait.carwash.UI.Activities.LocationActivity
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:CircleImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var location:TextView
    lateinit var save:Button
    lateinit var change_pass:TextView
    var lat = ""
    var lng = ""
    var result = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        location = findViewById(R.id.location)
        save = findViewById(R.id.save)
        change_pass = findViewById(R.id.change_pass)
        change_pass.setOnClickListener { startActivity(Intent(this,ChangePasswordActivity::class.java)) }
        title.text = getString(R.string.my_data)
        back.setOnClickListener { onBackPressed()
        finish()}
        location.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        save.setOnClickListener {
            if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError(location,getString(R.string.enter_location))){
                return@setOnClickListener
            }else{
                updateData()
            }
        }
        getData()
    }
    fun updateData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Profile(lang.appLanguage,"Bearer "+user.userData.token!!,name.text.toString(),phone.text.toString(),location.text.toString(),lat,lng)
                ?.enqueue(object: Callback<UserResponse> {
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                            call: Call<UserResponse>,
                            response: Response<UserResponse>
                    ) {
                        hideProgressDialog()
                        if(response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                user.userData = response.body()?.data!!
                                Glide.with(mContext!!).asBitmap().load(response.body()?.data?.avatar).into(image)
                                name.setText(response.body()?.data?.name)
                                phone.setText(response.body()?.data?.phone)
                                location.text = response.body()?.data?.address!!
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!

                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }
                })

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Profile(lang.appLanguage,"Bearer "+user.userData.token!!,null,null,null,null,null)
                ?.enqueue(object: Callback<UserResponse> {
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                            call: Call<UserResponse>,
                            response: Response<UserResponse>
                    ) {
                        hideProgressDialog()
                        if(response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                Glide.with(mContext!!).asBitmap().load(response.body()?.data?.avatar).into(image)
                                user.userData = response.body()?.data!!
                                name.setText(response.body()?.data?.name)
                                phone.setText(response.body()?.data?.phone)
                                location.text = response.body()?.data?.address!!
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!


                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }
                })
    }
    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.upload(lang.appLanguage,"Bearer "+user.userData.token,filePart)?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                        user.userData = response.body()?.data!!
                        Glide.with(mContext!!).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.setText(response.body()?.data?.name)
                        phone.setText(response.body()?.data?.phone)


                        location.text = response.body()?.data?.address!!
                        lat = response.body()?.data?.lat!!
                        lng = response.body()?.data?.lng!!

                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }else  if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext!!).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }
}