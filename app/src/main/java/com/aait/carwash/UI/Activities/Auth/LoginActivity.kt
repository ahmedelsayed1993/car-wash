package com.aait.carwash.UI.Activities.Auth

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.UserResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
//import com.aait.carwash.Models.UserResponse
//import com.aait.carwash.Network.Client
//import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Client.MainActivity
import com.aait.carwash.UI.Activities.Main.Provider.DeliveryActivity
//import com.aait.carwash.UI.Activities.Main.Client.MainActivity
import com.aait.carwash.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var phone: EditText
    lateinit var password: EditText
    lateinit var forgot_pass: TextView
    lateinit var login: Button
    lateinit var register: LinearLayout
    lateinit var skip:TextView
    var type = ""
    var deviceID = ""

    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })
        type = intent.getStringExtra("type")!!
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        skip = findViewById(R.id.skip)
        forgot_pass = findViewById(R.id.forgot)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        if (type.equals("user")){
            skip.visibility = View.VISIBLE
        }else{
            skip.visibility = View.GONE
        }
        skip.setOnClickListener { user.loginStatus = false
        startActivity(Intent(this, MainActivity::class.java))
        finish()}
        register.setOnClickListener {
            if (type.equals("user")) {
                startActivity(Intent(this, RegisterActivity::class.java))
            }else{
                startActivity(Intent(this, ProviderRegisterActivity::class.java))
            }}
        forgot_pass.setOnClickListener { startActivity(Intent(this,ForgotPasswordActivity::class.java)) }
        login.setOnClickListener {
            if(CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                Login()
            }
        }

    }

    fun Login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(phone.text.toString()
                ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.type!!.equals("user")) {
                            user.loginStatus = true
                            user.userData = response.body()?.data!!

                            val intent = Intent(this@LoginActivity, MainActivity::class.java)

                            startActivity(intent)
                            finish()

                        }else{
                            user.loginStatus = true
                            user.userData = response.body()?.data!!
                            if (response.body()?.data?.delivery==0){
                                startActivity(Intent(this@LoginActivity,DeliveryActivity::class.java))
                                finish()

                            }else{
                                startActivity(Intent(this@LoginActivity,com.aait.carwash.UI.Activities.Main.Provider.MainActivity::class.java))
                            }
//
//                            val intent = Intent(this@LoginActivity,com.aait.carwash.UI.Activities.Main.Delegate.MainActivity::class.java)
//
//                            startActivity(intent)
//                            finish()
                        }
//                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                        startActivity(intent)
//                        finish()
                    }else if (response.body()?.value.equals("2")){
                        val intent = Intent(this@LoginActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}