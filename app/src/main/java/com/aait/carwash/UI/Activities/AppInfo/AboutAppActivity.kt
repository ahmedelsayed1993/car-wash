package com.aait.carwash.UI.Activities.AppInfo

import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutAppActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_terms
     lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var terms:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        terms = findViewById(R.id.terms)
        back.setOnClickListener { onBackPressed()
            finish() }

        title.text = getString(R.string.about_app)
        getData()


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(lang.appLanguage)?.enqueue(object:
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        terms.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}