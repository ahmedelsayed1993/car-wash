package com.aait.carwash.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.R

class ChooseUserActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_choose_user
    lateinit var users:Button
    lateinit var delegate:Button

    override fun initializeComponents() {
        users = findViewById(R.id.user)
        delegate = findViewById(R.id.provider)
        users.setOnClickListener { val intent = Intent(this,IntroOneActivity::class.java)
            intent.putExtra("type","user")
            startActivity(intent)
            finish() }
        delegate.setOnClickListener { val intent = Intent(this,IntroOneActivity::class.java)
            intent.putExtra("type","provider")
            startActivity(intent)
            finish() }
    }
}