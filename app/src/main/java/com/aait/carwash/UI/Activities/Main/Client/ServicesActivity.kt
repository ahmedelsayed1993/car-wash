package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.HomeResponse
import com.aait.carwash.Models.ProviderDetailsModel
import com.aait.carwash.Models.ServicesModel
import com.aait.carwash.Models.ServicesResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.ServiceAdapter
import com.aait.carwash.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicesActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_services
    lateinit var back:ImageView
    lateinit var title:TextView

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var confirm:Button
    lateinit var serviceAdapter: ServiceAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var services = ArrayList<ServicesModel>()
    lateinit var servicesModel: ServicesModel
    lateinit var provider:ProviderDetailsModel
    override fun initializeComponents() {
        provider = intent.getSerializableExtra("provider") as ProviderDetailsModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        serviceAdapter = ServiceAdapter(mContext, services, R.layout.recycle_services)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        serviceAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = serviceAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        confirm.setOnClickListener {
            if (servicesModel.count==0){
                CommonUtil.makeToast(mContext,getString(R.string.choose_count))
            }else{
                val intent = Intent(this,ProductsActivity::class.java)
                intent.putExtra("service",servicesModel)
                intent.putExtra("provider",provider)
                startActivity(intent)

            }
        }
    }

    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
         showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Services(lang.appLanguage,provider.provider_id!!)?.enqueue(object:
                Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<ServicesResponse>,
                    response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            confirm.visibility = View.GONE

                        } else {
                            confirm.visibility = View.VISIBLE
                            servicesModel = response.body()?.data?.get(0)!!
                            serviceAdapter.updateAll(response.body()?.data!!)
                        }


                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.lay){
            serviceAdapter.selected = position
            services.get(position).checked = true
            serviceAdapter.notifyDataSetChanged()
            servicesModel = services.get(position)
            Log.e("service",Gson().toJson(servicesModel))
        }
        else if (view.id == R.id.plus){
            services.get(position).count = services.get(position).count!!+1
            serviceAdapter.notifyItemChanged(position)
            servicesModel = services.get(position)
            Log.e("service",Gson().toJson(servicesModel))
        }else if (view.id == R.id.minus){
            if (services.get(position).count!!<=0){

            }else{
                services.get(position).count = services.get(position).count!!-1
                serviceAdapter.notifyItemChanged(position)
            }
            servicesModel = services.get(position)
            Log.e("service",Gson().toJson(servicesModel))
        }

    }
}