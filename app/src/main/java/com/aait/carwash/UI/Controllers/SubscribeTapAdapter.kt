package com.aait.carwash.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.carwash.R
import com.aait.carwash.UI.Fragments.Client.CurrentFragment
import com.aait.carwash.UI.Fragments.Client.FinishedFragment
import com.aait.carwash.UI.Fragments.Client.PendingFragment


class SubscribeTapAdapter (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            PendingFragment()
        } else if (position == 1){
            CurrentFragment()
        }
        else {
            FinishedFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.pending_payment)
        }else if (position == 1){
            context.getString(R.string.current_orders)
        }
        else  {
            context.getString(R.string.finished_orders)
        }
    }
}
