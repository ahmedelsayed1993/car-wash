package com.aait.carwash.UI.Activities.Main.Client

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.TrackOrder
import com.aait.carwash.Models.UserOrderDetailsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_user_order_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var invoice:TextView
    lateinit var address:TextView
    lateinit var order_num:TextView
    lateinit var follow:Button
    lateinit var cancel:Button
    lateinit var rate:Button
    lateinit var reason:TextView
    lateinit var lay_reason: LinearLayout
    lateinit var phone:TextView
    var id = 0
    var phon = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        invoice = findViewById(R.id.invoice)
        address = findViewById(R.id.address)
        order_num = findViewById(R.id.order_num)
        follow = findViewById(R.id.follow)
        cancel = findViewById(R.id.cancel)
        rate = findViewById(R.id.rate)
        reason = findViewById(R.id.reason)
        phone = findViewById(R.id.phone)
        lay_reason = findViewById(R.id.lay_reason)
        getData()
        cancel.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.UserOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,"canceled")
                    ?.enqueue(object : Callback<UserOrderDetailsResponse> {
                        override fun onFailure(call: Call<UserOrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<UserOrderDetailsResponse>, response: Response<UserOrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                   CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                                    finish()

                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        invoice.setOnClickListener {
            val intent = Intent(this,InvoiceActivty::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.order_details)
        follow.setOnClickListener {
            Client.getClient()?.create(Service::class.java)?.Track(lang.appLanguage,"Bearer"+user.userData.token,id)
                    ?.enqueue(object : Callback<TrackOrder> {
                        override fun onFailure(call: Call<TrackOrder>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TrackOrder>, response: Response<TrackOrder>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    startActivity(
                                            Intent(
                                                    Intent.ACTION_VIEW,
                                                    Uri.parse("http://maps.google.com/maps?daddr="+response.body()?.data?.get(0)?.lat_provider!! + "," + response.body()?.data?.get(0)?.lng_provider!! )
                                            )
                                    )

                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
            }
        rate.setOnClickListener { val intent = Intent(this,RateActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent) }
        phone.setOnClickListener { getLocationWithPermission(phon) }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UserOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null)
                ?.enqueue(object : Callback<UserOrderDetailsResponse> {
                    override fun onFailure(call: Call<UserOrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<UserOrderDetailsResponse>, response: Response<UserOrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                Glide.with(mContext).load(response.body()?.data?.image).into(image)
                                name.text = response.body()?.data?.name
                                address.text = response.body()?.data?.address
                                order_num.text = getString(R.string.order_num)+":"+response.body()?.data?.order_id
                                phone.text = getString(R.string.phone_number)+":"+response.body()?.data?.phone
                                phon = response.body()?.data?.phone!!
                                lay_reason.visibility = View.GONE
                                if (response.body()?.data?.reason.equals("")){
                                    lay_reason.visibility = View.GONE
                                }else{
                                    lay_reason.visibility = View.VISIBLE
                                    reason.text = response.body()?.data?.reason
                                }
                                if (response.body()?.data?.status.equals("news")){
                                    cancel.visibility = View.VISIBLE
                                    follow.visibility = View.GONE
                                    rate.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("accepted")){
                                    cancel.visibility = View.VISIBLE
                                    follow.visibility = View.VISIBLE
                                    rate.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("finished")){
                                    cancel.visibility = View.GONE
                                    follow.visibility = View.GONE
                                    if (response.body()?.data?.rate==0) {
                                        rate.visibility = View.VISIBLE
                                    }else{
                                        rate.visibility = View.GONE
                                    }
                                }else{
                                    cancel.visibility = View.GONE
                                    follow.visibility = View.GONE
                                    rate.visibility = View.GONE
                                }

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }
}