package com.aait.carwash.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service

import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Auth.LoginActivity
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IntroOneActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_one

    lateinit var title:TextView
    lateinit var desc:TextView
    lateinit var next:ImageView
    lateinit var skip:TextView
    var type = ""
    override fun initializeComponents() {
        type = intent.getStringExtra("type")!!
        title = findViewById(R.id.title)
        desc = findViewById(R.id.desc)
        next = findViewById(R.id.next)
        skip = findViewById(R.id.skip)
        next.setOnClickListener { val intent = Intent(this,IntroTwoActivity::class.java)
            intent.putExtra("type",type)
            startActivity(intent)
            finish()
        }
        skip.setOnClickListener { val intent = Intent(this,LoginActivity::class.java)
            intent.putExtra("type",type)
            startActivity(intent)
            finish()
        }

        title.text = getString(R.string.app_name)
        getData()
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(lang.appLanguage)?.enqueue(object:
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        desc.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}