package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.FincialModel

import com.aait.carwash.R

class FinancialAdapter (context: Context, data: MutableList<FincialModel>, layoutId: Int) :
        ParentRecyclerAdapter<FincialModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name.text = listModel.user_name
        viewHolder.num.text = listModel.order_id.toString()
        viewHolder.phone.text = listModel.user_phone
        viewHolder.total.text = listModel.price+mcontext.getString(R.string.rs)
        if (position%2==0){
            viewHolder.total.textColor = mcontext.resources.getColor(R.color.colorPrimaryDark)
            viewHolder.num.textColor = mcontext.resources.getColor(R.color.colorPrimaryDark)
            viewHolder.name.textColor = mcontext.resources.getColor(R.color.colorPrimaryDark)
            viewHolder.phone.textColor = mcontext.resources.getColor(R.color.colorPrimaryDark)
            viewHolder.invoice.textColor = mcontext.resources.getColor(R.color.colorPrimaryDark)
        }else{
            viewHolder.total.textColor = mcontext.resources.getColor(R.color.colorPrimary)
            viewHolder.num.textColor = mcontext.resources.getColor(R.color.colorPrimary)
            viewHolder.name.textColor = mcontext.resources.getColor(R.color.colorPrimary)
            viewHolder.phone.textColor = mcontext.resources.getColor(R.color.colorPrimary)
            viewHolder.invoice.textColor = mcontext.resources.getColor(R.color.colorPrimary)
        }
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var num=itemView.findViewById<TextView>(R.id.num)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var phone = itemView.findViewById<TextView>(R.id.phone)
        internal var total = itemView.findViewById<TextView>(R.id.total)
        internal var invoice = itemView.findViewById<TextView>(R.id.invoice)


    }
}