package com.aait.carwash.UI.Activities.Main.Provider

import android.app.DatePickerDialog
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AddOfferActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_add_offer
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var name:EditText
    lateinit var price:EditText
    lateinit var discount:EditText
    lateinit var time_from:TextView
    lateinit var time_to:TextView
    lateinit var content:EditText
    lateinit var confirm:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        price = findViewById(R.id.price)
        discount = findViewById(R.id.discount)
        time_from = findViewById(R.id.time_from)
        time_to = findViewById(R.id.time_to)
        content = findViewById(R.id.content)
        confirm = findViewById(R.id.confirm)
        time_from.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                if (m<10){
                    if (dayOfMonth<10){
                        time_from.setText("" + year + "-" +"0"+ m + "-" +"0"+ dayOfMonth)
                    }else{
                        time_from.setText("" + year + "-" +"0"+ m + "-" + dayOfMonth)
                    }
                }else{
                    if (dayOfMonth<10){
                        time_from.setText("" + year + "-" + m + "-" +"0"+ dayOfMonth)
                    }else{
                        time_from.setText("" + year + "-" + m + "-" + dayOfMonth)
                    }
                }
//                date.setText("" + year + "-" + m + "-" + dayOfMonth)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        time_to.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                if (m<10){
                    if (dayOfMonth<10){
                        time_to.setText("" + year + "-" +"0"+ m + "-" +"0"+ dayOfMonth)
                    }else{
                        time_to.setText("" + year + "-" +"0"+ m + "-" + dayOfMonth)
                    }
                }else{
                    if (dayOfMonth<10){
                        time_to.setText("" + year + "-" + m + "-" +"0"+ dayOfMonth)
                    }else{
                        time_to.setText("" + year + "-" + m + "-" + dayOfMonth)
                    }
                }
//                date.setText("" + year + "-" + m + "-" + dayOfMonth)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.add_offer)

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.package_name))||
                    CommonUtil.checkEditError(price,getString(R.string.price))||
                    CommonUtil.checkEditError(discount,getString(R.string.discount))||
                    CommonUtil.checkTextError(time_from,getString(R.string.from))||
                    CommonUtil.checkTextError(time_to,getString(R.string.to))||
                    CommonUtil.checkEditError(content,getString(R.string.package_content))){
                return@setOnClickListener
            }else{
                   Add()
            }
        }

    }

    fun Add(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOffer("Bearer"+user.userData.token,lang.appLanguage,name.text.toString()
                ,price.text.toString(),discount.text.toString(),time_from.text.toString(),time_to.text.toString(),content.text.toString()
        )?.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}