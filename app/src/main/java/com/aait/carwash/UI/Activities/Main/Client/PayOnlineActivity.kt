package com.aait.carwash.UI.Activities.Main.Client

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient




import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayOnlineActivity : ParentActivity() {

    internal var web: WebView? = null
    internal var id: Int = 0
    internal var type = ""


     override val layoutResource: Int
        get() = R.layout.activity_online



    @SuppressLint("SetJavaScriptEnabled")
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        type = intent.getStringExtra("type")!!
        web = findViewById(R.id.web)
        web!!.settings.javaScriptEnabled = true

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            web!!.settings.mixedContentMode = 0
            web!!.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            web!!.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            web!!.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        Log.e(
            "link",
            "https://carwash.aait-sa.com/online-payment?order="+id+"&methods="+type
        )
        web!!.webViewClient = MyWebVew()
        web!!.loadUrl("https://carwash.aait-sa.com/online-payment?order="+id+"&methods="+type)
      //  web!!.loadUrl("https://checkout.payments.tap.company/?mode=page&token=615d62242a6b380ebd344f9f")
        Log.e("ur", web!!.url!!)


//        if (web!!.url!!.contains("https://carwash.aait-sa.com/payment-success")) {
//            Pay()
//
//        }else if (web!!.url!!.contains("https://carwash.aait-sa.com/payment-fail")){
//
//            onBackPressed()
//            finish()
//            CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
//        }
    }

    protected fun hideInputType(): Boolean {
        return false
    }

    inner class MyWebVew : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: String): Boolean {
            Log.e("link",request)
           // view.loadUrl(request)
            if (request.contains("https://carwash.aait-sa.com/payment-fail")) {
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        onBackPressed()
                        finish()
                    }, 3000)
                }, 3000)

            } else if (request.contains("https://carwash.aait-sa.com/payment-success")) {


                Pay()

            }else {
                Log.e("request",request)
                view.loadUrl(request)


            }
            return true
        }
    }

    fun Pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Pay("Bearer"+user.userData.token,lang.appLanguage,id,"online"
        )?.enqueue(object : Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@PayOnlineActivity,BackActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}
