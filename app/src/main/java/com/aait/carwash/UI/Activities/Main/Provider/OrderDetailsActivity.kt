package com.aait.carwash.UI.Activities.Main.Provider

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.GPS.GPSTracker
import com.aait.carwash.GPS.GpsTrakerListener
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Models.OrderDetailsResponse
import com.aait.carwash.Models.ProviderDetailsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Activities.Main.Client.InvoiceActivty
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.DialogUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class OrderDetailsActivity:ParentActivity(),GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_provider_order_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var invoice:TextView
    lateinit var order_num:TextView
    lateinit var phone:TextView
    lateinit var type:TextView
    lateinit var time:TextView
    lateinit var payment:TextView
    lateinit var date:TextView
    lateinit var notes:TextView
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var finish:Button
    lateinit var cancel:Button
    lateinit var follow:Button
    var id = 0
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 60000L
    var lat = ""
    var lng  = ""
    var phon = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        invoice = findViewById(R.id.invoice)
        order_num = findViewById(R.id.order_num)
        phone = findViewById(R.id.phone)
        type = findViewById(R.id.type)
        time = findViewById(R.id.time)
        payment = findViewById(R.id.payment)
        date = findViewById(R.id.date)
        notes = findViewById(R.id.notes)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        finish = findViewById(R.id.finish)
        cancel = findViewById(R.id.cancel)
        follow = findViewById(R.id.follow)
        timer = Timer()
        title.text = getString(R.string.order_details)
        back.setOnClickListener { onBackPressed()
        finish()}
        invoice.setOnClickListener {
            val intent = Intent(this,InvoiceActivty::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }
        getData()
        refuse.setOnClickListener {
            val intent = Intent(this,RefuseActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }
        follow.setOnClickListener {
            getLocationWithPermission()
            startActivity(
                Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lng )
                )
        ) }
        phone.setOnClickListener { getLocationWithPermission(phon) }
        accept.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.ProviderOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,"accepted")
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    val timerTask = object : TimerTask() {
                                        override fun run() {
                                            runOnUiThread {
                                                Log.e("rrr","rrrrrr")
                                                getLocationWithPermission() }
                                        }
                                    }


                                    timer.schedule(timerTask, noDelay, everyFiveSeconds)
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        finish.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.ProviderOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,"finished")
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    timer.cancel()
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        cancel.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.ProviderOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,"deleted")
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    timer.cancel()
                                    startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,null)
                ?.enqueue(object : Callback<OrderDetailsResponse> {
                    override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.image).into(image)
                                name.text = response.body()?.data?.user_name
                                phon = response.body()?.data?.phone!!
                                address.text = response.body()?.data?.address
                                date.text = getString(R.string.date)+":"+response.body()?.data?.date
                                time.text = getString(R.string.time)+":"+response.body()?.data?.time
                                order_num.text = getString(R.string.order_num)+":"+response.body()?.data?.order_id
                                phone.text = getString(R.string.phone_number)+":"+response.body()?.data?.phone
                                type.text = getString(R.string.subscribe)+":"+response.body()?.data?.subtype
                                payment.text = getString(R.string.payment_method)+":"+response.body()?.data?.payment_method
                                notes.text = getString(R.string.notes)+":"+response.body()?.data?.desc
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                if (response.body()?.data?.status.equals("news")){
                                    accept.visibility = View.VISIBLE
                                    refuse.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    cancel.visibility = View.GONE
                                    follow.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("accepted")){
                                    accept.visibility = View.GONE
                                    refuse.visibility = View.GONE
                                    follow.visibility = View.VISIBLE
                                    finish.visibility = View.VISIBLE
                                    cancel.visibility = View.VISIBLE
                                    val timerTask = object : TimerTask() {
                                        override fun run() {
                                            runOnUiThread {
                                                Log.e("rrr","rrrrrr")
                                                getLocationWithPermission() }
                                        }
                                    }


                                    timer.schedule(timerTask, noDelay, everyFiveSeconds)
                                }else{
                                    accept.visibility = View.GONE
                                    refuse.visibility = View.GONE
                                    finish.visibility = View.GONE
                                    cancel.visibility = View.GONE
                                    follow.visibility = View.GONE
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(lat:String,lng:String){


        Client.getClient()?.create(Service::class.java)?.OrderTrack(lang.appLanguage,"Bearer"+user.userData.token,lat,lng,id)?.enqueue(object:
                Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
            ) {
                hideProgressDialog()

                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                    }else{
                     //   CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                            mContext as Activity, PermissionUtils.CALL_PHONE,
                            300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }
}