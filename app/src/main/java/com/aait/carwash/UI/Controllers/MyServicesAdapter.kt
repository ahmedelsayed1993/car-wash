package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.MyServicesModel
import com.aait.carwash.Models.SizesModel
import com.aait.carwash.Models.Type
import com.aait.carwash.R
import com.bumptech.glide.Glide

class MyServicesAdapter (context: Context, data: MutableList<MyServicesModel>, layoutId: Int) :
        ParentRecyclerAdapter<MyServicesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)

        Glide.with(mcontext).asBitmap().load(listModel.size?.image).into(viewHolder.image)
        viewHolder.name.text = listModel.size?.name
        viewHolder.productsAdapter = MyTypesAdapter(mcontext,viewHolder.productModels,R.layout.recycler_my_types)
        viewHolder.types.layoutManager = LinearLayoutManager(mcontext,
                LinearLayoutManager.HORIZONTAL,false)
        viewHolder.types.adapter = viewHolder.productsAdapter
        viewHolder.productsAdapter.updateAll(listModel.typeCleaning!!)


        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.delete.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.edit.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var selected = itemView.findViewById<ImageView>(R.id.selected)
        internal var delete = itemView.findViewById<ImageView>(R.id.delete)
        internal var edit = itemView.findViewById<ImageView>(R.id.edit)
        internal var types = itemView.findViewById<RecyclerView>(R.id.types)
        lateinit var productsAdapter:MyTypesAdapter
        internal var productModels = ArrayList<Type>()


    }
}