package com.aait.carwash.UI.Controllers

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.TimeModel
import com.aait.carwash.Models.TypesModel
import com.aait.carwash.R
import com.bumptech.glide.Glide

class TimesAdapter (context: Context, data: MutableList<TimeModel>, layoutId: Int) :
    ParentRecyclerAdapter<TimeModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)

        listModel.selected = (selected==position)
        viewHolder.time.text = listModel.time
       if (listModel.status.equals("booked")){

           viewHolder.time.background = mcontext.resources.getDrawable(R.drawable.red_shap)
       }else if (listModel.status.equals("available")){

           if (listModel.selected==null){
               viewHolder.time.background = mcontext.resources.getDrawable(R.drawable.green_shap)
           }else if (listModel.selected!!){
               viewHolder.time.background = mcontext.resources.getDrawable(R.drawable.orange_shape)
           }else{
               viewHolder.time.background = mcontext.resources.getDrawable(R.drawable.green_shap)
           }
       }

        viewHolder.time.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {


        internal var time = itemView.findViewById<TextView>(R.id.time)




    }
}