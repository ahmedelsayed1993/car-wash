package com.aait.carwash.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.GPS.GPSTracker
import com.aait.carwash.GPS.GpsTrakerListener
import com.aait.carwash.Models.*
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import com.aait.carwash.Utils.DialogUtil
import com.aait.carwash.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class ProviderDetailsActivity:ParentActivity(),GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_provider_details

    lateinit var back:ImageView
    lateinit var offer:LinearLayout
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var distance:TextView
    lateinit var desc:TextView
    lateinit var rating:RatingBar
    lateinit var wash:LinearLayout
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var id = 0
    lateinit var providersModel: ProviderDetailsModel
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        distance = findViewById(R.id.distance)
        desc = findViewById(R.id.desc)
        rating = findViewById(R.id.rating)
        wash = findViewById(R.id.wash)
        offer = findViewById(R.id.offer)
        back.setOnClickListener { onBackPressed()
        finish()}

        getLocationWithPermission(null)
        wash.setOnClickListener { val intent = Intent(this,ServicesActivity::class.java)
        intent.putExtra("provider",providersModel)
        startActivity(intent)}
        offer.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, OffersActivity::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }else{
                  CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
            }
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true

    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(lat:String,lng:String){

         showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Provider(lang.appLanguage,id,lat,lng)?.enqueue(object:
                Callback<ProviderDetailsResponse> {
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<ProviderDetailsResponse>,
                    response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()

                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        providersModel = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.text = response.body()?.data?.name
                        address.text = response.body()?.data?.address
                        distance.text = response.body()?.data?.distance
                        desc.text = response.body()?.data?.description
                        rating.rating = response.body()?.data?.review!!.toFloat()
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
        Client.getClient()?.create(Service::class.java)?.CheckOffer()?.enqueue(object :Callback<CheckOfferResponse>{
            override fun onResponse(call: Call<CheckOfferResponse>, response: Response<CheckOfferResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            offer.visibility = View.VISIBLE
                        }else{
                            offer.visibility = View.GONE
                        }

                    }
                }
            }

            override fun onFailure(call: Call<CheckOfferResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }
        })
    }
}