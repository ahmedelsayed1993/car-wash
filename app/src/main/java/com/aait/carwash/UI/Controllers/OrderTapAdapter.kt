package com.aait.carwash.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.carwash.R
import com.aait.carwash.UI.Fragments.Provider.CurrentFragment
import com.aait.carwash.UI.Fragments.Provider.FInishedFragment
import com.aait.carwash.UI.Fragments.Provider.CancelledFragment

class OrderTapAdapter (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentFragment()
        } else if (position == 1){
            FInishedFragment()
        }
        else {
            CancelledFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.current_orders)
        }else if (position == 1){
            context.getString(R.string.finished_orders)
        }
        else  {
            context.getString(R.string.cancelled_orders)
        }
    }
}
