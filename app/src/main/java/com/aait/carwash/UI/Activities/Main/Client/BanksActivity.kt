package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Listeners.OnItemClickListener
import com.aait.carwash.Models.BanksModel
import com.aait.carwash.Models.BanksResponse
import com.aait.carwash.Models.OffersModel
import com.aait.carwash.Models.OffersResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.UI.Controllers.BanksAdapter
import com.aait.carwash.UI.Controllers.OfferAdapter
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BanksActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_banks
    lateinit var back: ImageView
    lateinit var title: TextView

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var banksAdapter: BanksAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var offers = ArrayList<BanksModel>()
    var id = 0
    lateinit var offersModel: OffersModel
    lateinit var confirm:Button
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        offersModel = intent.getSerializableExtra("package") as OffersModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        confirm = findViewById(R.id.confirm)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)

        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.bank_accounts)
        banksAdapter = BanksAdapter(mContext, offers, R.layout.recycle_banks)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)

        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = banksAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        confirm.setOnClickListener {
            val intent = Intent(this,TransferActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("package",offersModel)
            startActivity(intent)
        }
    }

    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Banks("Bearer"+user.userData.token,lang.appLanguage,id)?.enqueue(object:
                Callback<BanksResponse> {
            override fun onFailure(call: Call<BanksResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<BanksResponse>,
                    response: Response<BanksResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


                        } else {

                            banksAdapter.updateAll(response.body()?.data!!)

                        }


                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }



    override fun onItemClick(view: View, position: Int) {

    }
}