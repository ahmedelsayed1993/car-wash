package com.aait.carwash.UI.Activities.Main.Provider

import android.content.Context
import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.TermsResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeliveryActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_delivery_price
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var delivery:EditText
    lateinit var confirm:Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        delivery = findViewById(R.id.delivery)
        confirm = findViewById(R.id.confirm)
        title.text = getString(R.string.delivery)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        Delivery(null)
        confirm.setOnClickListener { Delivery(delivery.text.toString()) }

    }

    fun Delivery(deliver:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Delivery("Bearer"+user.userData.token,lang.appLanguage,deliver)
                ?.enqueue(object : Callback<TermsResponse> {
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                delivery.setText(response.body()?.data)
                                if (deliver== null){

                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}