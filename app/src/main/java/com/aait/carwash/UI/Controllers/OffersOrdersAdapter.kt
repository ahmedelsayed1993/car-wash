package com.aait.carwash.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentRecyclerAdapter
import com.aait.carwash.Base.ParentRecyclerViewHolder
import com.aait.carwash.Models.OffersOrdersModel
import com.aait.carwash.Models.OrdersModel
import com.aait.carwash.R
import com.bumptech.glide.Glide

class OffersOrdersAdapter (context: Context, data: MutableList<OffersOrdersModel>, layoutId: Int) :
        ParentRecyclerAdapter<OffersOrdersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.user_name)
        viewHolder.offer!!.setText(questionModel.offer_name)
        viewHolder.amount.text = questionModel.amount.toString()+mcontext.getString(R.string.rs)
        Glide.with(mcontext).asBitmap().load(questionModel.image).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var offer = itemView.findViewById<TextView>(R.id.offer)
        internal var amount = itemView.findViewById<TextView>(R.id.amount)



    }
}