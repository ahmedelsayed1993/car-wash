package com.aait.carwash.UI.Activities.Main.Client

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.OrderResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R
import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InvoiceActivty : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_invice
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var size: TextView
    lateinit var count: TextView
    lateinit var type: TextView
    lateinit var price: TextView
    lateinit var lay1: LinearLayout
    lateinit var ratio: TextView
    lateinit var lay2: LinearLayout
    lateinit var discount: TextView
    lateinit var lay3: LinearLayout
    lateinit var price_after_discount: TextView
    lateinit var tax: TextView
    lateinit var price_after_tax: TextView
    lateinit var delivery: TextView
    lateinit var total: TextView
    lateinit var pay: Button
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        size = findViewById(R.id.size)
        count = findViewById(R.id.count)
        type = findViewById(R.id.type)
        price = findViewById(R.id.price)
        lay1 = findViewById(R.id.lay1)
        ratio = findViewById(R.id.ratio)
        lay2 = findViewById(R.id.lay2)
        discount = findViewById(R.id.discount)
        lay3 = findViewById(R.id.lay3)
        price_after_discount = findViewById(R.id.price_after_discount)
        tax = findViewById(R.id.tax)
        price_after_tax = findViewById(R.id.price_after_tax)
        delivery = findViewById(R.id.delivery)
        total = findViewById(R.id.total)
        pay = findViewById(R.id.pay)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.invoice)
        getData()
        pay.visibility = View.GONE

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Bill("Bearer"+user.userData.token,lang.appLanguage,id)
                ?.enqueue(object : Callback<OrderResponse> {
                    override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<OrderResponse>, response: Response<OrderResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                size.text = response.body()?.data?.size
                                count.text = response.body()?.data?.count
                                type.text = response.body()?.data?.type_clean
                                price.text = response.body()?.data?.price+getString(R.string.rs)
                                if (response.body()?.data?.discount_percentage.equals("")){
                                    lay1.visibility = View.GONE
                                    lay2.visibility = View.GONE
                                    lay3.visibility = View.GONE
                                }else{
                                    lay1.visibility = View.VISIBLE
                                    lay2.visibility = View.VISIBLE
                                    lay3.visibility = View.VISIBLE
                                }
                                ratio.text = response.body()?.data?.discount_percentage+"%"
                                discount.text = response.body()?.data?.discount+getString(R.string.rs)
                                price_after_discount.text = response.body()?.data?.price_after_discount+getString(R.string.rs)
                                tax.text = response.body()?.data?.tax+"%"
                                price_after_tax.text = response.body()?.data?.price_tax+getString(R.string.rs)
                                delivery.text = response.body()?.data?.delivery+getString(R.string.rs)
                                total.text = getString(R.string.total)+response.body()?.data?.total+getString(R.string.rs)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}