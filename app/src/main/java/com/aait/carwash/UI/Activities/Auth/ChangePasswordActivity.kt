package com.aait.carwash.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.carwash.Base.ParentActivity
import com.aait.carwash.Models.BaseResponse
import com.aait.carwash.Network.Client
import com.aait.carwash.Network.Service
import com.aait.carwash.R

import com.aait.carwash.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_change_password
      lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var password:EditText
    lateinit var notification:ImageView
    lateinit var new_pass:EditText

    lateinit var confirm_new:EditText

    lateinit var save:Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        password = findViewById(R.id.password)
        new_pass = findViewById(R.id.new_pass)
        confirm_new = findViewById(R.id.confirm_new)
        save = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}

        title.text = getString(R.string.change_password)


        save.setOnClickListener { if ( CommonUtil.checkEditError(password,getString(R.string.old_password))||
            CommonUtil.checkLength(password,getString(R.string.password_length),6)||
            CommonUtil.checkEditError(new_pass,getString(R.string.new_pass))||
                CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
            CommonUtil.checkEditError(confirm_new,getString(R.string.confirm_new_pass)) ) {
            return@setOnClickListener
        }else{
            if (!new_pass.text.toString().equals(confirm_new.text.toString())){
                confirm_new.error = getString(R.string.password_not_match)
            }else{
                NewPassword()
            }
        }
        }

    }
    fun NewPassword(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resetPassword(lang.appLanguage,"Bearer "+user.userData.token,password.text.toString(),new_pass.text.toString())?.enqueue(
            object : Callback<BaseResponse> {
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            onBackPressed()
                            finish()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                        }
                    }
                }
            }
        )

    }
}