package com.aait.carwash.Network

import android.widget.ImageView
import com.aait.carwash.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {
    @POST("About-Us")
    fun About(@Header("lang") lang: String): Call<TermsResponse>

    @POST("Privacy")
    fun Terms(@Header("lang") lang: String): Call<TermsResponse>
    @POST("Question")
    fun Questions(@Header("lang") lang: String): Call<QuestionsResponse>

    @POST("Complaint-Post")
    fun Complaint(@Header("lang") lang:String,
                  @Query("name") name:String?,
                  @Query("phone") phone:String?,
                  @Query("email") email:String?,
                  @Query("address_complaint") subject:String?,
                  @Query("body_complaint") message:String?):Call<BaseResponse>
    @POST("Contact-Us-Post")
    fun ContactUs(@Header("lang") lang:String,
                  @Query("name") name:String?,
                  @Query("phone") phone:String?,
                  @Query("email") email:String?,
                  @Query("address_message") subject:String?,
                  @Query("body_message") message:String?):Call<BaseResponse>

    @POST("contract")
    fun Contract():Call<TermsResponse>

    @POST("Contact-Us")
    fun Contact(@Header("lang") lang: String):Call<ContactUsResponse>

    @FormUrlEncoded
    @POST("sign-Up-User")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("address") address:String,
               @Field("lat") lat:String,
               @Field("lng") lng:String,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String): Call<UserResponse>

    @FormUrlEncoded
    @POST("code-sign-up")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resent-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("change-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("old_password") current_password:String,
                      @Field("new_password") password:String):Call<BaseResponse>

    @FormUrlEncoded
    @POST("sign-In")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String):Call<UserResponse>
    @FormUrlEncoded
    @POST("forget-password")
    fun ForGot(@Field("phone") phone:String,
               @Header("lang") lang:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("reset-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Field("password") password:String,
                @Field("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>

    @POST("Home")
    fun Home(@Header("lang") lang: String,
             @Query("lat") lat:String,
             @Query("lng") lng:String):Call<HomeResponse>


    @POST("Search")
    fun Search(@Header("lang") lang: String,
                 @Query("lat") lat:String,
                 @Query("lng") lng:String,
               @Query("search") search:String
    ):Call<SearchResponse>
    @POST("Provider-Details")
    fun Provider(@Header("lang") lang: String,
               @Query("provider_id") provider_id:Int,
               @Query("lat") lat:String,
               @Query("lng") lng:String
    ):Call<ProviderDetailsResponse>

    @POST("Provider-Service")
    fun Services(@Header("lang") lang: String,
                 @Query("provider_id") provider_id:Int):Call<ServicesResponse>

    @POST("Provider-TypeCleanings")
    fun Products(@Header("lang") lang: String,
                 @Query("provider_id") provider_id:Int,
                 @Query("service_id") service_id:Int):Call<ProductResponse>

    @POST("Add-Order")
    fun AddOrder(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("provider_id") provider_id:Int,
    @Query("service_id") service_id:Int,
    @Query("count") count:Int,
    @Query("type_clean_id") type_clean_id:Int,
    @Query("lat") lat:String,
    @Query("lng") lng:String,
    @Query("address") address: String,
    @Query("user_name") user_name:String,
    @Query("user_phone") user_phone:String,
    @Query("date") date:String,
    @Query("time") time:String,
    @Query("desc") desc:String,
    @Query("offer_id") offer_id:Int?):Call<OrderResponse>

    @POST("Check-User-Offer")
    fun Check(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
              @Query("provider_id") provider_id:Int):Call<TermsResponse>

    @POST("Orders-User")
    fun Orders(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("status") status:String):Call<OrdersResponse>
    @POST("Provider-Orders")
    fun ProviderOrders(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
               @Query("status") status:String):Call<OrdersResponse>

    @POST("Order-Bill")
    fun Bill(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("order_id") order_id:Int):Call<OrderResponse>

    @POST("Order-Payment")
    fun Pay(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
            @Query("order_id") order_id:Int,
            @Query("type") type:String):Call<BaseResponse>
    @POST("Setting")
    fun Switch(@Header("Authorization") Authorization:String,
               @Query("notify") switch:Int?):Call<NotifyResponse>

    @POST("Provider-Offers")
    fun Offers(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("provider_id") provider_id: Int):Call<OffersResponse>

    @POST("Bank-Account")
    fun Banks(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
              @Query("provider_id") provider_id: Int):Call<BanksResponse>

    @POST("bank_accounts")
    fun Accounts(@Header("Authorization") Authorization:String,@Header("lang") lang: String):Call<BankResponse>

    @Multipart
    @POST("Add-offer-user")
    fun AddOffer(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                 @Query("provider_id") provider_id: Int,
                 @Query("offer_id") offer_id:Int,
                 @Query("bank_name") bank_name:String,
                 @Query("account_holder_name") account_holder_name:String,
                 @Query("amount") amount:String,
                 @Part image:MultipartBody.Part):Call<BaseResponse>

    @Multipart
    @POST("sign-Up-Provider")
    fun SignUpProvider(@Header("lang") lang: String,
    @Query("phone") phone:String,
    @Query("name") name:String,
    @Query("email") email:String,
    @Query("lat") lat:String,
    @Query("lng") lng: String,
    @Query("address") address: String,
    @Part identification_card:MultipartBody.Part,
    @Part shop_photo:MultipartBody.Part,
     @Part shop_license:MultipartBody.Part,
     // @Part contract:MultipartBody.Part,
    @Query("commercial") commercial:String?,
    @Query("bank_account") bank_account:String,
    @Query("bank_name") bank_name:String,
    @Query("iban_number") iban_number:String,
    @Query("time_from") time_from:String?,
    @Query("time_to") time_to:String?,
    @Query("days") days:String?,
    @Query("password") password:String,
    @Query("device_id") device_id:String,
    @Query("device_type")  device_type:String):Call<UserResponse>
    @Multipart
    @POST("sign-Up-Provider")
    fun SignUpProviderwithout(@Header("lang") lang: String,
                       @Query("phone") phone:String,
                       @Query("name") name:String,
                       @Query("email") email:String,
                       @Query("lat") lat:String,
                       @Query("lng") lng: String,
                       @Query("address") address: String,
                       @Part identification_card:MultipartBody.Part,
                           //   @Part contract:MultipartBody.Part,
                       @Query("commercial") commercial:String?,
                       @Query("bank_account") bank_account:String,
                       @Query("bank_name") bank_name:String,
                       @Query("iban_number") iban_number:String,
                       @Query("time_from") time_from:String?,
                       @Query("time_to") time_to:String?,
                       @Query("days") days:String?,
                       @Query("password") password:String,
                       @Query("device_id") device_id:String,
                       @Query("device_type")  device_type:String):Call<UserResponse>
    @Multipart
    @POST("sign-Up-Provider")
    fun SignUpProviderwithpoto(@Header("lang") lang: String,
                       @Query("phone") phone:String,
                       @Query("name") name:String,
                       @Query("email") email:String,
                       @Query("lat") lat:String,
                       @Query("lng") lng: String,
                       @Query("address") address: String,
                       @Part identification_card:MultipartBody.Part,
                       @Part shop_photo:MultipartBody.Part,
                             //  @Part contract:MultipartBody.Part,
                       @Query("commercial") commercial:String?,
                       @Query("bank_account") bank_account:String,
                       @Query("bank_name") bank_name:String,
                       @Query("iban_number") iban_number:String,
                       @Query("time_from") time_from:String?,
                       @Query("time_to") time_to:String?,
                       @Query("days") days:String?,
                       @Query("password") password:String,
                       @Query("device_id") device_id:String,
                       @Query("device_type")  device_type:String):Call<UserResponse>
    @Multipart
    @POST("sign-Up-Provider")
    fun SignUpProviderwithLie(@Header("lang") lang: String,
                       @Query("phone") phone:String,
                       @Query("name") name:String,
                       @Query("email") email:String,
                       @Query("lat") lat:String,
                       @Query("lng") lng: String,
                       @Query("address") address: String,
                       @Part identification_card:MultipartBody.Part,
                       @Part shop_license:MultipartBody.Part,
                           //   @Part contract:MultipartBody.Part,
                       @Query("commercial") commercial:String?,
                       @Query("bank_account") bank_account:String,
                       @Query("bank_name") bank_name:String,
                       @Query("iban_number") iban_number:String,
                       @Query("time_from") time_from:String?,
                       @Query("time_to") time_to:String?,
                       @Query("days") days:String?,
                       @Query("password") password:String,
                       @Query("device_id") device_id:String,
                       @Query("device_type")  device_type:String):Call<UserResponse>
    @Multipart
    @POST("settlements_payment")
    fun Pay(@Header("Authorization") Authorization:String,
            @Header("lang") lang: String,
            @Query("name") name:String,
            @Query("bank_name") bank_name:String,
            @Query("price") amount:String,
            @Part image:MultipartBody.Part):Call<BanksResponse>
    @POST("Delivery")
    fun Delivery(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("delivery") delivery:String?):Call<TermsResponse>

    @POST("logout")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Header("lang") lang:String):Call<BaseResponse>

    @POST("Add-Service")
    fun Services(@Header("Authorization") Authorization:String,@Header("lang") lang: String):Call<AddServiceResponse>

    @POST("Add-Service-Store")
    fun AddService(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("size_id") size_id:Int,
    @Query("type_clean") type_clean:String):Call<BaseResponse>

    @POST("services")
    fun MyServices(@Header("Authorization") Authorization:String,@Header("lang") lang: String):Call<MyServicesResponse>

    @POST("Offers")
    fun MyOffers(@Header("Authorization") Authorization:String,@Header("lang") lang: String):Call<OffersResponse>

    @POST("Service-Delete")
    fun DeleteService(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String,
                      @Query("service_id") service_id:Int):Call<BaseResponse>
    @POST("Delete-Offer")
    fun DeleteOffer(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String,
                      @Query("offer_id") service_id:Int):Call<BaseResponse>

    @POST("Add-Offer")
    fun AddOffer(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("name") name:String,
    @Query("price") price:String,
    @Query("discount") discount:String,
    @Query("start") start:String,
    @Query("end") end:String,
    @Query("content") content:String):Call<BaseResponse>
    @POST("Update-Offer")
    fun EditOffer(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                  @Query("offer_id") offer_id: Int,
                 @Query("name") name:String,
                 @Query("price") price:String,
                 @Query("discount") discount:String,
                 @Query("start") start:String,
                 @Query("end") end:String,
                 @Query("content") content:String):Call<BaseResponse>

    @POST("Orders-Offers")
    fun MyOffersOrders(@Header("Authorization") Authorization:String,@Header("lang") lang: String):Call<OffersOrdersResponse>

    @POST("Offer-Subscriber")
    fun OfferSub(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
    @Query("subscriber_id") subscriber_id:Int,
    @Query("status") status:String?):Call<OfferSubscribeResponse>

    @POST("profile-provider-update")
    fun ProviderProfile(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                        @Query("phone") phone:String?,
                        @Query("name") name:String?,
                        @Query("manger_name") manger_name:String?,
                        @Query("owner_name") owner_name:String?,
                        @Query("email") email:String?,
                        @Query("lat") lat:String?,
                        @Query("lng") lng: String?,
                        @Query("address") address: String?,
                        @Query("commercial") commercial:String?,
                        @Query("time_from") time_from:String?,
                        @Query("time_to") time_to:String?,
                        @Query("days") days:String?):Call<UserResponse>
    @Multipart
    @POST("profile-provider-update")
    fun ProviderID(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                        @Part identification_card:MultipartBody.Part):Call<UserResponse>
    @Multipart
    @POST("profile-provider-update")
    fun ProviderLiece(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                      @Part shop_license:MultipartBody.Part):Call<UserResponse>
    @Multipart
    @POST("profile-provider-update")
    fun ProviderShop(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                     @Part shop_photo:MultipartBody.Part):Call<UserResponse>

    @Multipart
    @POST("profile-provider-update")
    fun ProviderAvatar(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                     @Part avatar:MultipartBody.Part):Call<UserResponse>

    @POST("Provider-Order")
    fun ProviderOrderDetails(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                             @Query("order_id") order_id:Int,
                             @Query("reason_refuse") reason_refuse:String?,
                             @Query("status") status:String?):Call<OrderDetailsResponse>
    @POST("Order-User")
    fun UserOrderDetails(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                             @Query("order_id") order_id:Int,
                             @Query("status") status:String?):Call<UserOrderDetailsResponse>
    @POST("Notifications")
    fun Notification(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String):Call<NotificationResponse>
    @POST("Delete-Notify")
    fun delete(
            @Header("Authorization") Authorization:String,
            @Header("lang") lang:String,
            @Query("notify_id") notification_id:String):Call<BaseResponse>

    @POST("Edit-Offer")
    fun EditOffer(@Header("Authorization") Authorization:String,
                  @Header("lang") lang:String,
                  @Query("offer_id") offer_id:Int):Call<EditOfferResponse>

    @POST("Review")
    fun Reviews(@Header("Authorization") Authorization:String,
                @Header("lang") lang:String):Call<RatesResponse>

    @POST("profile-user-update")
    fun Profile(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String,
                @Query("name") name:String?,
                @Query("phone") phone: String?,
                @Query("address") address:String?,
                @Query("lat") lat: String?,
                @Query("lng") lng:String?
    ):Call<UserResponse>

    @Multipart
    @POST("profile-user-update")
    fun upload(@Header("lang") lang: String,
               @Header("Authorization") Authorization:String,
               @Part avatat:MultipartBody.Part):Call<UserResponse>

    @POST("Order-Tracking-Update")
    fun OrderTrack(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
    @Query("lat") lat:String,
    @Query("lng") lng:String,
    @Query("order_id") order_id: Int):Call<BaseResponse>

    @POST("Order-Tracing-View")
    fun Track(@Header("lang") lang: String,
              @Header("Authorization") Authorization:String,
              @Query("order_id") order_id: Int
    ):Call<TrackOrder>
    @POST("delete-order")
    fun Delete(@Header("lang") lang: String,
               @Header("Authorization") Authorization:String,
               @Query("order_id") order_id: Int):Call<TermsResponse>
    @POST("Add-Review")
    fun AddRate(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String,@Query("order_id") order_id: Int,
    @Query("review") review:Float):Call<BaseResponse>

    @POST("financial_accounts")
    fun Finacial(@Header("Authorization") Authorization:String,
                 @Header("lang") lang: String):Call<FinacialAccountModel>

    @POST("Edit-Service")
    fun EditService(@Header("Authorization") Authorization:String,
                    @Header("lang") lang: String,
                    @Query("service_id") service_id:Int):Call<EditServiceResponse>

    @POST("Service-Update")
    fun UpdateServicce(@Header("Authorization") Authorization:String,
                       @Header("lang") lang: String,
                       @Query("service_id") service_id:Int,
    @Query("type_clean") type_clean:String):Call<BaseResponse>

    @GET("check-offer")
    fun CheckOffer():Call<CheckOfferResponse>

    @POST("provider-date-time")
    fun getTimes(@Header("lang") lang:String,
                 @Query("provider_id") provider_id: Int,
                 @Query("date") date: String):Call<TimeResponse>
}