package com.aait.carwash.Listeners

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
