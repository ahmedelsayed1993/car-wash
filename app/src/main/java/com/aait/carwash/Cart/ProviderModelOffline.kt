package com.aait.carwash.Cart

import androidx.room.*

@Entity(indices = [Index(value = ["service_id"], unique = true)])

class ProviderModelOffline(var provider_id: Int,
                           var provider_name: String?,
                           var provider_image: String?,
                           var address:String?,
                           var type_id:Int?,
                           var name:String?,
                           var service_id: Int,
                           var count:Int?
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}