package com.aait.carwash.Models

import java.io.Serializable

class ServicesResponse:BaseResponse(),Serializable {
    var data:ArrayList<ServicesModel>?=null
}