package com.aait.carwash.Models

import java.io.Serializable

class QuestionModel:Serializable {

    var content:String?=null
    var answer:String?=null
}