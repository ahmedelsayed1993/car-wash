package com.aait.carwash.Models

import java.io.Serializable

class UserModel:Serializable {
    var token:String?=null
    var name:String?=null
    var phone:String?=null
    var email:String?=null
    var code:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var delivery:Int?=null
    var type:String?=null
    var active:Int?=null
    var confirm:Int?=null
    var lat:String?=null
    var lng:String?=null
    var address:String?=null
    var status:String?=null
    var manger_name:String?=null
    var owner_name:String?=null
    var identification_card:String?=null
    var shop_photo:String?=null
    var shop_license:String?=null
    var commercial:String?=null
    var time_from:String?=null
    var time_to:String?=null
    var days:ArrayList<String>?=null

}