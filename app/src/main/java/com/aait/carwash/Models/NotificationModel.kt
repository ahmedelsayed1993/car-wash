package com.aait.carwash.Models

import java.io.Serializable

class NotificationModel:Serializable {
    var id:String?=null
    var content:String?=null
    var created:String?=null
    var type:String?=null
    var order_id:String?=null
}