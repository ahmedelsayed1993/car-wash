package com.aait.carwash.Models

import java.io.Serializable

class NotificationResponse:BaseResponse(),Serializable {
    var data:ArrayList<NotificationModel>?=null
}