package com.aait.carwash.Models

import java.io.Serializable

class OffersOrdersResponse:BaseResponse(),Serializable {
    var data:ArrayList<OffersOrdersModel>?=null
}