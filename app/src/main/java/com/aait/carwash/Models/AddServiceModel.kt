package com.aait.carwash.Models

import java.io.Serializable

class AddServiceModel:Serializable {
    var size:ArrayList<SizesModel>?=null
    var typeCleanings:ArrayList<TypesModel>?=null
}