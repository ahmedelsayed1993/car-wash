package com.aait.carwash.Models

import java.io.Serializable

class OffersModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var start:String?=null
    var end:String?=null
    var discount:String?=null
    var content:String?=null
    var price:String?=null
}