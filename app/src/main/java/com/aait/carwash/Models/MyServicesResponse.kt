package com.aait.carwash.Models

import java.io.Serializable

class MyServicesResponse:BaseResponse(),Serializable {
    var data:ArrayList<MyServicesModel>?=null
}