package com.aait.carwash.Models

import java.io.Serializable

class TrackOrderModel:Serializable {
    var lat_user:String?=null
    var lng_user:String?=null
    var lat_provider:String?=null
    var lng_provider:String?=null
}