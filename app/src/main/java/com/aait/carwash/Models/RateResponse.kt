package com.aait.carwash.Models

import java.io.Serializable

class RateResponse:Serializable {
    var info:RateModel?=null
    var reviews:ArrayList<RateModel>?=null
}