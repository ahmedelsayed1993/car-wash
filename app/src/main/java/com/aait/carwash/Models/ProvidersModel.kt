package com.aait.carwash.Models

import java.io.Serializable

class ProvidersModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var address:String?=null
    var distance:String?=null
    var lat:String?=null
    var lng:String?=null
}