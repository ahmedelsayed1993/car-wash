package com.aait.carwash.Models

import java.io.Serializable

class RateModel:Serializable {
    var name:String?=null
    var image:String?=null
    var review:Float?=null
}