package com.aait.carwash.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var user_name:String?=null
    var image:String?=null
    var address:String?=null
    var order_id:Int?=null
    var date:String?=null
    var time:String?=null
    var phone:String?=null
    var payment_method:String?=null
    var desc:String?=null
    var subtype:String?=null
    var status:String?=null
    var rate:Int?=null
    var lat:String?=null
    var lng:String?=null
}