package com.aait.carwash.Models

import java.io.Serializable

class TimeModel:Serializable {
    var time:String?=null
    var status:String?=null
    var selected:Boolean?=null
}