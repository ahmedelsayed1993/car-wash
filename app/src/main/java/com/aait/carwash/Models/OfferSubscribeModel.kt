package com.aait.carwash.Models

import java.io.Serializable

class OfferSubscribeModel:Serializable {
    var user_name:String?=null
    var image_user:String?=null
    var offer_name:String?=null
    var bank_name:String?=null
    var account_holder_name:String?=null
    var image_subscriber:String?=null
    var amount:String?=null
    var status:String?=null
}