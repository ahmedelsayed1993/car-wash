package com.aait.carwash.Models

import java.io.Serializable

class ProductModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var price:String?=null
    var sort:Int?=null
    var checked:Boolean?=null
}