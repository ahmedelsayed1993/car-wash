package com.aait.carwash.Models

import java.io.Serializable

class OrderModel:Serializable {
    var order_id:Int?=null
    var size:String?=null
    var count:String?=null
    var type_clean:String?=null
    var price:String?=null
    var discount_percentage:String?=null
    var discount:String?=null
    var price_after_discount:String?=null
    var tax:String?=null
    var price_tax:String?=null
    var delivery:String?=null
    var total:String?=null
}