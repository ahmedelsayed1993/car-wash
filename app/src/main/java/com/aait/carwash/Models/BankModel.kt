package com.aait.carwash.Models

import java.io.Serializable

class BankModel:Serializable {
    var id:Int?=null
    var bank_name:String?=null
    var bank_account:String?=null
    var account_number:String?=null
    var account_ibn:String?=null
}