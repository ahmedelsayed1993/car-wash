package com.aait.carwash.Models

import java.io.Serializable

class AddServiceResponse:BaseResponse(),Serializable {
    var data:AddServiceModel?=null
}