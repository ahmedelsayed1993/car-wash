package com.aait.carwash.Models

import java.io.Serializable

class ProviderDetailsModel :Serializable {
    var provider_id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var address:String?=null
    var review:String?=null
    var description:String?=null
    var distance:String?=null
}