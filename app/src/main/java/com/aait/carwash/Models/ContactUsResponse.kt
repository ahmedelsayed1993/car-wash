package com.aait.carwash.Models

import java.io.Serializable

class ContactUsResponse:BaseResponse(),Serializable {
    var data:ContactUsModel?=null
    var socials:ArrayList<SocialModel>?=null
}