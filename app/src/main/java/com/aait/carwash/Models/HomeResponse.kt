package com.aait.carwash.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var sliders:ArrayList<String>?=null
    var data:ArrayList<ProvidersModel>?=null
}