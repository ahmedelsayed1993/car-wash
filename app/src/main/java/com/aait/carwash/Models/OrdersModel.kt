package com.aait.carwash.Models

import java.io.Serializable

class OrdersModel:Serializable {
    var order_id:Int?=null
    var provider_id:Int?=null
    var name:String?=null
    var address:String?=null
    var image:String?=null
}