package com.aait.carwash.Models

import com.aait.carwash.UI.Controllers.UserOrderDetailsModel
import java.io.Serializable

class UserOrderDetailsResponse:BaseResponse(),Serializable {
    var data: UserOrderDetailsModel?=null
}