package com.aait.carwash.Models

import java.io.Serializable

class Type:Serializable {
    var type_id:Int?=null
    var price:Int?=null
    var image:String?=null
    var name:String?=null

    constructor(type_id: Int?, price: Int?) {
        this.type_id = type_id
        this.price = price
    }
}