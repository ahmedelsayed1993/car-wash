package com.aait.carwash.Models

import java.io.Serializable

class ServicesModel:Serializable {
    var service_id:Int?=null
    var name:String?=null
    var image:String?=null
    var count:Int?=null
    var checked:Boolean?=null
}