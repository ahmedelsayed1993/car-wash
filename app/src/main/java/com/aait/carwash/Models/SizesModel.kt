package com.aait.carwash.Models

import java.io.Serializable

class SizesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var checked:Boolean?=null
}