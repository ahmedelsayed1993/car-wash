package com.aait.carwash.Models

import java.io.Serializable

class BanksModel:Serializable {
    var bank_name:String?=null
    var account_holder_name:String?=null
    var bank_account:String?=null
    var iban_number:String?=null
}