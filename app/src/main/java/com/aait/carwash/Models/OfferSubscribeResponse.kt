package com.aait.carwash.Models

import java.io.Serializable

class OfferSubscribeResponse:BaseResponse(),Serializable {
    var data:OfferSubscribeModel?=null
}