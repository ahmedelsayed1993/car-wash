package com.aait.carwash.Models

import java.io.Serializable

class OffersOrdersModel:Serializable {
    var subscriber_id:Int?=null
    var amount:Double?=null
    var offer_name:String?=null
    var image:String?=null
    var user_name:String?=null
}